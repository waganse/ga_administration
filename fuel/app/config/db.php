<?php
/**
 * The database settings. These get merged with the global settings.
 */

return array(
  'default' => array(
    'type'   => 'mysqli',
    'connection' => array(
      'hostname'   => '127.0.0.1',
      // 'hostname'   => 'cwd-web',
      'database'   => 'ga_admin',
      'username'   => 'ga_admin',
      'password'   => 'ga_admin',
      'persistent' => false,
      'compress' => false,
      'port' => '8889', // Please comment out in case of the connection to cwd-web
    ),
    'table_prefix' => '',
    'charset'   => 'utf8',
    'enable_cache' => true,
    'caching'   => false,
    'profiling' => true,
  ),
);
