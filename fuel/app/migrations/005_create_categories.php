<?php

namespace Fuel\Migrations;

class Create_categories
{
	public function up()
	{
		\DBUtil::create_table('categories', array(
			'id' => array('constraint' => 11, 'type' => 'int', 'auto_increment' => true, 'unsigned' => true),
            'name_ja' => array('constraint' => 50, 'type' => 'varchar'),
            'name_en' => array('constraint' => 50, 'type' => 'varchar'),
            'name_cn' => array('constraint' => 50, 'type' => 'varchar'),
            'name_tw' => array('constraint' => 50, 'type' => 'varchar'),
			'label' => array('constraint' => 50, 'type' => 'varchar'),
			'status' => array('constraint' => 11, 'type' => 'int'),
			'remarks' => array('type' => 'text', 'null' => true),
			'last_updated_by' => array('constraint' => 11, 'type' => 'int', 'unsigned' => true),
			'deleted_at' => array('constraint' => 11, 'type' => 'int', 'null' => true),
			'created_at' => array('constraint' => 11, 'type' => 'int', 'null' => true),
			'updated_at' => array('constraint' => 11, 'type' => 'int', 'null' => true),

        ), array('id'),true,'InnoDB',null,
            array(
                array(
                    'key' => 'last_updated_by',
                    'reference' => array(
                        'table' => 'users',
                        'column' => 'id'
                    )
                ),
            )
        );
	}

	public function down()
	{
		\DBUtil::drop_table('categories');
	}
}