<?php

namespace Fuel\Migrations;

class Create_models
{
	public function up()
	{
		\DBUtil::create_table('models', array(
			'id' => array('constraint' => 11, 'type' => 'int', 'auto_increment' => true, 'unsigned' => true),
            'name_ja' => array('constraint' => 50, 'type' => 'varchar'),
            'name_en' => array('constraint' => 50, 'type' => 'varchar'),
            'name_kana' => array('constraint' => 50, 'type' => 'varchar'),
			'img' => array('constraint' => 255, 'type' => 'varchar'),
			'status' => array('constraint' => 11, 'type' => 'int', 'default' => '1'),
			'remarks' => array('type' => 'text', 'null' => true),
			'last_updated_by' => array('constraint' => 11, 'type' => 'int', 'null' => true),
			'deleted_at' => array('constraint' => 11, 'type' => 'int', 'null' => true),
			'created_at' => array('constraint' => 11, 'type' => 'int', 'null' => true),
			'updated_at' => array('constraint' => 11, 'type' => 'int', 'null' => true),
        ), array('id'),true,'InnoDB',null,
            array(
                array(
                    'key' => 'last_updated_by',
                    'reference' => array(
                        'table' => 'users',
                        'column' => 'id'
                    )
                ),
            )
        );
	}

	public function down()
	{
		\DBUtil::drop_table('models');
	}
}