<?php

namespace Fuel\Migrations;

class Create_items
{
	public function up()
	{
		\DBUtil::create_table('items', array(
			'id' => array('constraint' => 11, 'type' => 'int', 'auto_increment' => true, 'unsigned' => true),
            'name_ja' => array('constraint' => 255, 'type' => 'varchar'),
            'name_en' => array('constraint' => 255, 'type' => 'varchar'),
            'name_cn' => array('constraint' => 255, 'type' => 'varchar'),
			'name_tw' => array('constraint' => 255, 'type' => 'varchar'),
            'price_ja' => array('constraint' => 11, 'type' => 'int'),
            'price_en' => array('constraint' => 11, 'type' => 'int'),
            'price_cn' => array('constraint' => 11, 'type' => 'int'),
			'price_tw' => array('constraint' => 11, 'type' => 'int'),
			'url' => array('constraint' => 255, 'type' => 'varchar'),
			'img' => array('constraint' => 255, 'type' => 'varchar'),
            'brand_id' => array('constraint' => 11, 'type' => 'int', 'unsigned' => true),
            'order' => array('constraint' => 11, 'type' => 'int', 'unsigned' => true),
			'order_sub' => array('constraint' => 11, 'type' => 'int', 'unsigned' => true),
            'translate_status' => array('constraint' => 11, 'type' => 'int'),
			'sold_status' => array('constraint' => 11, 'type' => 'int'),
			'remarks' => array('type' => 'text', 'null' => true),
            'last_updated_by' => array('constraint' => 11, 'type' => 'int', 'unsigned' => true),
			'deleted_at' => array('constraint' => 11, 'type' => 'int', 'null' => true),
			'created_at' => array('constraint' => 11, 'type' => 'int', 'null' => true),
			'updated_at' => array('constraint' => 11, 'type' => 'int', 'null' => true),

        ), array('id'),true,'InnoDB',null,
            array(
                array(
                    'key' => 'brand_id',
                    'reference' => array(
                        'table' => 'brands',
                        'column' => 'id'
                    )
                ),
                array(
                    'key' => 'last_updated_by',
                    'reference' => array(
                        'table' => 'users',
                        'column' => 'id'
                    )
                ),
            )
        );
	}

	public function down()
	{
		\DBUtil::drop_table('items');
	}
}