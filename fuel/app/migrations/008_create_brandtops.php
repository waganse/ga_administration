<?php

namespace Fuel\Migrations;

class Create_brandTops
{
	public function up()
	{
		\DBUtil::create_table('brand_top', array(
			'id' => array('constraint' => 11, 'type' => 'int', 'auto_increment' => true, 'unsigned' => true),
            'brand_id' => array('constraint' => 11, 'type' => 'int', 'unsigned' => true),
            'model_id' => array('constraint' => 11, 'type' => 'int', 'unsigned' => true),
            'order' => array('constraint' => 11, 'type' => 'int', 'unsigned' => true),
            'photo_status' => array('constraint' => 11, 'type' => 'int', 'unsigned' => true),
            'global_status' => array('constraint' => 11, 'type' => 'int', 'unsigned' => true),
			'remarks' => array('type' => 'text', 'null' => true),
			'last_updated_by' => array('constraint' => 11, 'type' => 'int', 'unsigned' => true),
			'deleted_at' => array('constraint' => 11, 'type' => 'int', 'null' => true),
			'created_at' => array('constraint' => 11, 'type' => 'int', 'null' => true),
			'updated_at' => array('constraint' => 11, 'type' => 'int', 'null' => true),

        ), array('id'),true,'InnoDB',null,
            array(
                array(
                    'key' => 'brand_id',
                    'reference' => array(
                        'table' => 'brands',
                        'column' => 'id'
                    )
                ),
                array(
                    'key' => 'model_id',
                    'reference' => array(
                        'table' => 'models',
                        'column' => 'id'
                    )
                ),
                array(
                    'key' => 'last_updated_by',
                    'reference' => array(
                        'table' => 'users',
                        'column' => 'id'
                    )
                ),
            )
        );
	}

	public function down()
	{
		\DBUtil::drop_table('brand_top');
	}
}