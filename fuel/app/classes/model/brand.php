<?php

class Model_Brand extends \Orm\Model_Soft
{
	protected static $_properties = array(
		'id',
		'name_ja',
		'name_en',
		'label',
		'img',
		'img_sp',
		'up_status',
		'new_status',
		'global_status',
		'remarks',
		'last_updated_by',
		'deleted_at',
		'created_at',
		'updated_at',
	);

	protected static $_observers = array(
		'Orm\Observer_CreatedAt' => array(
			'events' => array('before_insert'),
			'mysql_timestamp' => false,
		),
		'Orm\Observer_UpdatedAt' => array(
			'events' => array('before_update'),
			'mysql_timestamp' => false,
		),
	);

	protected static $_has_many = array(
		'items' => array(
		     'model_to' => 'Model_Item',
		     'key_from' => 'id',
		     'key_to' => 'brand_id',
		     'cascade_save' => true,
		     'cascade_delete' => false,
		)
	);

	public static function validate($factory)
	{
		$val = Validation::forge($factory);
		$val->add_field('name_ja', 'Name (JP)', 'required|max_length[50]');
		$val->add_field('name_en', 'Name (EN)', 'max_length[50]');
		$val->add_field('label', 'Label', 'max_length[50]');
		$val->add_field('img', 'Img URL', 'max_length[255]');
		$val->add_field('img_sp', 'Img URL (SP)', 'max_length[255]');

		return $val;
	}

	protected static $_soft_delete = array(
		'mysql_timestamp' => false,
	);

	protected static $_table_name = 'brands';

}
