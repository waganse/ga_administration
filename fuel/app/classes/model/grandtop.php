<?php

class Model_Grandtop extends \Orm\Model_Soft
{
	protected static $_properties = array(
		'id',
		'brand_id',
		'order',
		'remarks',
		'last_updated_by',
		'deleted_at',
		'created_at',
		'updated_at',
	);

	protected static $_observers = array(
		'Orm\Observer_CreatedAt' => array(
			'events' => array('before_insert'),
			'mysql_timestamp' => false,
		),
		'Orm\Observer_UpdatedAt' => array(
			'events' => array('before_update'),
			'mysql_timestamp' => false,
		),
	);

	public static function validate($factory)
	{
		$val = Validation::forge($factory);

		$val->add_field('brand_id', 'Brand', 'required');

		return $val;
	}

	protected static $_soft_delete = array(
		'mysql_timestamp' => false,
	);

	protected static $_table_name = 'grand_top';

}
