<?php

class Model_Model extends \Orm\Model_Soft
{
	protected static $_properties = array(
		'id',
		'name_ja',
		'name_en',
		'name_kana',
		'img',
		'status',
		'remarks',
		'last_updated_by',
		'deleted_at',
		'created_at',
		'updated_at',
	);

	protected static $_observers = array(
		'Orm\Observer_CreatedAt' => array(
			'events' => array('before_insert'),
			'mysql_timestamp' => false,
		),
		'Orm\Observer_UpdatedAt' => array(
			'events' => array('before_update'),
			'mysql_timestamp' => false,
		),
	);

	public static function validate($factory)
	{
		$val = Validation::forge($factory);
		$val->add_field('name_ja', 'Name (JP)', 'required|max_length[50]');
		$val->add_field('name_en', 'Name (EN)', 'max_length[50]');
		$val->add_field('name_kana', 'Name (hiragana)', 'required|max_length[50]');
		$val->add_field('img', 'Img URL', 'valid_url|max_length[255]');

		return $val;
	}

	protected static $_soft_delete = array(
		'mysql_timestamp' => false,
	);

	protected static $_table_name = 'models';

}
