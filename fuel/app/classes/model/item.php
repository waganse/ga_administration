<?php

class Model_Item extends \Orm\Model_Soft
{
	protected static $_properties = array(
		'id',
		'name_ja',
		'name_en',
		'name_cn',
		'name_tw',
		'price_ja',
		'price_en',
		'price_cn',
		'price_tw',
		'url',
		'img',
		'brand_id',
		'order',
		'order_sub',
		'translate_status',
		'sold_status',
		'remarks',
		'last_updated_by',
		'deleted_at',
		'created_at',
		'updated_at',
	);

	protected static $_observers = array(
		'Orm\Observer_CreatedAt' => array(
			'events' => array('before_insert'),
			'mysql_timestamp' => false,
		),
		'Orm\Observer_UpdatedAt' => array(
			'events' => array('before_update'),
			'mysql_timestamp' => false,
		),
	);

	public static function validate($factory)
	{
		$val = Validation::forge($factory);
		$val->add_field('name_ja', 'Name (JP)', 'required|max_length[255]');
		$val->add_field('name_en', 'Name (EN)', 'max_length[255]');
		$val->add_field('name_cn', 'Name (CN)', 'max_length[255]');
		$val->add_field('name_tw', 'Name (TW)', 'max_length[255]');
		$val->add_field('price_ja', 'Price (JPY)', 'max_length[11]|valid_string[numeric]');
		$val->add_field('price_en', 'Price (USD)', 'max_length[11]|valid_string[numeric]');
		$val->add_field('price_cn', 'Price (CHY)', 'max_length[11]|valid_string[numeric]');
		$val->add_field('price_tw', 'Price (TWY)', 'max_length[11]|valid_string[numeric]');
		$val->add_field('url', 'URL', 'max_length[255]');
		$val->add_field('img', 'IMAGE', 'max_length[255]|valid_url');

		return $val;
	}
	protected static $_soft_delete = array(
		'mysql_timestamp' => false,
	);

	protected static $_table_name = 'items';

}
