<?php

class Model_User extends \Orm\Model
{
	protected static $_properties = array(
		'id',
		'username',
		'password',
		'group',
		'email',
		'last_login',
		'login_hash',
		'profile_fields',
		'created_at',
		'updated_at',
	);

	protected static $_observers = array(
		'Orm\Observer_CreatedAt' => array(
			'events' => array('before_insert'),
			'mysql_timestamp' => false,
		),
		'Orm\Observer_UpdatedAt' => array(
			'events' => array('before_update'),
			'mysql_timestamp' => false,
		),
	);

	public static function validate($factory)
	{
		$val = Validation::forge($factory);

		$val->add_field('username', 'User Name', 'required|max_length[50]');
		$val->add_field('email', 'Email', 'required|valid_email|max_length[50]');
		$val->add_field('password', 'Password', 'required|min_length[8]|max_length[16]');
		$val->add_field('password_2', 'Password (confirm)', 'required|match_field[password]|min_length[8]|max_length[16]');

		return $val;
	}

	public static function validate_update($factory)
	{
		$val = Validation::forge($factory);

		$val->add_field('email', 'Email', 'required|valid_email|max_length[50]');

		return $val;
	}

	public static function validate_account($factory)
	{
		$val = Validation::forge($factory);

		$val->add_field('old_password', 'Old password', 'required|min_length[8]|max_length[16]');
		$val->add_field('password', 'Password', 'required|min_length[8]|max_length[16]');
		$val->add_field('password_2', 'Password (confirm)', 'required|match_field[password]|min_length[8]|max_length[16]');

		return $val;
	}

	protected static $_table_name = 'users';

}
