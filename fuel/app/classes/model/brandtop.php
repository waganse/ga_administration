<?php

class Model_Brandtop extends \Orm\Model_Soft
{
	protected static $_properties = array(
		'id',
		'brand_id',
		'model_id',
		'img',
		'order',
		'photo_status',
		'remarks',
		'last_updated_by',
		'deleted_at',
		'created_at',
		'updated_at',
	);

	protected static $_observers = array(
		'Orm\Observer_CreatedAt' => array(
			'events' => array('before_insert'),
			'mysql_timestamp' => false,
		),
		'Orm\Observer_UpdatedAt' => array(
			'events' => array('before_update'),
			'mysql_timestamp' => false,
		),
	);

	public static function validate($factory)
	{
		$val = Validation::forge($factory);

		$val->add_field('model_id', 'Model', 'required');
		$val->add_field('img', 'Img URL', 'max_length[255]|valid_url');

		return $val;
	}

	protected static $_soft_delete = array(
		'mysql_timestamp' => false,
	);

	protected static $_table_name = 'brand_top';

}
