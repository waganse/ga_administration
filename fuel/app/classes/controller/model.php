<?php

class Controller_Model extends Controller_BaseHybrid
{
    public $data = array();
    public $status_arr = array('active', 'inactive');

    public function before()
    {
        parent::before();
        $this->data['users'] = Model_User::find('all', array());
    }

    public function action_index()
    {
        $data = $this->data;

        $data['data'] = Model_Model::find('all', array(
            'order_by' => array('status' => 'asc', 'name_kana' => 'asc'),
        ));

        $data['cnt'] = count($data['data']);

        foreach ($data['data'] as $i => $item) {
            $item->img_tooltip = ($item->img)? $item->img : Config::get('base_url').'assets/img/'.Constants::LINK_NOIMAGE;
            $item->status_str = $this->status_arr[$item->status];

            $item->update_user = $data['users'][$item->last_updated_by]->username;
            $item->recent_update_class = ($item->updated_at > time() - Constants::UPDATE_THRESHOLD_SEC)? 'new' : '';
            $item->updated_at = ($item->updated_at)? Date::forge($item->updated_at)->format(Constants::TIME_FORMAT) : '';

            $data['data'][$i] = $item;
        }

        $this->template->title = 'Models | '.Constants::PAGE_NAME;
        $this->template->content = View_Twig::forge('model/index', $data);
    }

    public function action_create()
    {
        !Auth::check() and Response::redirect('admin');

        $message = '';
        $data = $this->data;
        $item = new stdClass;

        if (Input::method() == 'POST')
        {
            $val = Model_Model::validate('create');

            if ($val->run())
            {
                $item = Model_Model::forge(array(
                    'name_ja' => Input::post('name_ja'),
                    'name_en' => Input::post('name_en'),
                    'name_kana' => Input::post('name_kana'),
                    'img' => Input::post('img'),
                    'status' => (int) Input::post('status'),
                    'remarks' => Input::post('remarks'),
                    'last_updated_by' => $this->current_user->id,
                ));

                try {
                    $item->save();

                    // Email notification
                    parent::sendmail(array(
                        'subject' => Constants::MAIL_SUBJECT_CREATE_MODEL,
                        'data' => array(
                            'Name (JP)' => ($item->name_ja)? $item->name_ja : '-',
                            'Name (EN)' => ($item->name_en)? $item->name_en : '-',
                            'Name (Kana)' => ($item->name_kana)? $item->name_kana : '-',
                            'Image URL' => ($item->img)? $item->img : '-',
                            'Status' => $this->status_arr[$item->status],
                            'Notes' => ($item->remarks)? $item->remarks : '-',
                        ),
                    ));

                    Response::redirect('model');
                }
                catch(Exception $e) {
                    $message = $e->getMessage();
                }
            }
            else
            {
                $item->name_ja = $val->validated('name_ja');
                $item->name_en = $val->validated('name_en');
                $item->name_kana = $val->validated('name_kana');
                $item->img = $val->validated('img');
                $item->status = (int) Input::post('status');
                $item->remarks = Input::post('remarks');
                $item->last_updated_by = $this->current_user->id;

                $message = $val->show_errors();
            }
        }
        $item->status_arr = $this->status_arr;

        $data['data'] = $item;
        $data['error'] = $message;
        $this->template->title = "NEW MODEL | ".Constants::PAGE_NAME;
        $this->template->content = View_Twig::forge('model/create', $data, false);
    }

    public function action_edit($id = null)
    {
        !Auth::check() and Response::redirect('admin');

        $message = '';
        $data = $this->data;
        $item = new stdClass;

        is_null($id) and Response::redirect('model');

        if ( ! $item = Model_Model::find($id))
        {
            $message = 'Could not find item #'.$id;
            Response::redirect('model');
        }

        $val = Model_Model::validate('edit');

        if ($val->run())
        {
            $item->name_ja = Input::post('name_ja');
            $item->name_en = Input::post('name_en');
            $item->name_kana = Input::post('name_kana');
            $item->img = Input::post('img');
            $item->status = (int) Input::post('status');
            $item->remarks = Input::post('remarks');
            $item->last_updated_by = $this->current_user->id;

            try {
                $item->save();

                // Email notification
                parent::sendmail(array(
                    'subject' => Constants::MAIL_SUBJECT_EDIT_MODEL,
                    'data' => array(
                        'Name (JP)' => ($item->name_ja)? $item->name_ja : '-',
                        'Name (EN)' => ($item->name_en)? $item->name_en : '-',
                        'Name (Kana)' => ($item->name_kana)? $item->name_kana : '-',
                        'Image URL' => ($item->img)? $item->img : '-',
                        'Status' => $this->status_arr[$item->status],
                        'Notes' => ($item->remarks)? $item->remarks : '-',
                    ),
                ));

                Response::redirect('model');
            }
            catch (Exception $e) {
                $message = $e->getMessage();
            }
        }
        else
        {
            if (Input::method() == 'POST')
            {
                $item->name_ja = $val->validated('name_ja');
                $item->name_en = $val->validated('name_en');
                $item->name_kana = $val->validated('name_kana');
                $item->img = $val->validated('img');
                $item->status = (int) Input::post('status');
                $item->remarks = Input::post('remarks');
                $item->last_updated_by = $this->current_user->id;

                $message = $val->show_errors();
            }
        }
        $item->status_arr = $this->status_arr;

        $data['data'] = $item;
        $data['error'] = $message;
        $this->template->title = "MODEL EDIT | ".Constants::PAGE_NAME;
        $this->template->content = View_Twig::forge('model/edit', $data, false);
    }

    public function action_delete($id = null)
    {
        !Auth::check() and Response::redirect('admin');

        $message = '';

        is_null($id) and Response::redirect('model');

        try {
            $item = Model_Model::find($id);
            $item->delete();

            // Email notification
            parent::sendmail(array(
                'subject' => Constants::MAIL_SUBJECT_DELETE_MODEL,
                'data' => array(
                    'Name (JP)' => ($item->name_ja)? $item->name_ja : '-',
                    'Name (EN)' => ($item->name_en)? $item->name_en : '-',
                    'Name (Kana)' => ($item->name_kana)? $item->name_kana : '-',
                    'Image URL' => ($item->img)? $item->img : '-',
                    'Status' => $this->status_arr[$item->status],
                    'Notes' => ($item->remarks)? $item->remarks : '-',
                ),
            ));

            Response::redirect('model');
        }
        catch (Exception $e) {
            $message = $e->getMessage();
        }

        Response::redirect('model');
    }

    // ここからはRESTAPI。使用していないけど、将来必要なら。。未テスト
    public function get_data()
    {
        $data = $this->data;
        $res = array(
            'cnt' => 0,
            'items' => array(),
        );

        $data['data'] = Model_Model::find('all', array(
            'order_by' => array('name_ja' => 'asc'),
        ));

        $res['cnt'] = count($data['data']);

        foreach ($data['data'] as $i => $item) {
            $item = $item->to_array();

            $item['last_updated_by'] = $data['users'][$item['last_updated_by']]->username;

            $item['status'] = ($item['status'])? true : false;

            Arr::delete($item, array('brand_id',));
            array_push($res['items'], parent::parseString2Numeric($item));
        }

        $this->response($res, 200);
    }

    public function post_data()
    {
        $message = '';
        $code = 0;
        $props = Input::post();

        try {
            $new = Model_Model::forge($props);
            $new->save();

            $message = 'success';
            $code = 200;
        }
        catch (Exception $e) {
            $message = $e->getMessage();
            $code = 500;
        }

        $this->response(array('code'=>$code, 'message'=>$message));
    }

    public function put_data($id = null)
    {
    }

    public function delete_data($id = null)
    {
    }
}