<?php
abstract class Controller_BaseHybrid extends Controller_Hybrid {
    public $template = Constants::TEMPLATE_VIEW;

    public function before()
    {
        parent::before();

        $this->current_user = null;

        if (!Auth::check())
        {
            Response::redirect('admin');
        }

        foreach (\Auth::verified() as $driver)
        {
            if (($id = $driver->get_user_id()) !== false)
            {
                $this->current_user = Model\Auth_User::find($id[1]);
            }
            break;
        }

        // Set a global variable so views can use it
        View::set_global('current_user', $this->current_user);

        // Top cordination
        View::set_global('cord_count', Model_Grandtop::count());

        // Models
        View::set_global('model_count', Model_Model::count());

        // Brands
        $brands = Model_Brand::find('all', array(
            'order_by' => array('name_en' => 'asc'),
        ));
        foreach ($brands as $key => $brand)
        {
            $brand->cord_count = Model_Brandtop::count(array( 'where' => array('brand_id' => $key) ));
            $brand->item_count = Model_Item::count(array( 'where' => array('brand_id' => $key) ));
        }
        View::set_global('all_brands', $brands);

        View::set_global('token', Security::fetch_token());
        View::set_global('token_name', Config::get('security.csrf_token_key'));
        View::set_global('token_script', Security::js_fetch_token());

        View::set_global('css', Asset::css('app.css'));
        View::set_global('js', Asset::js('app.js'));
        View::set_global('svg', Config::get('base_url').'assets/img/svg-symbols.svg');
        View::set_global('ect_api', Config::get('base_url').'export/json/data');
        View::set_global('build_api', Config::get('base_url').'build/data');

        View::set_global('link_base', Uri::base(false));
        View::set_global('link_login', Uri::base(false) . Constants::LINK_LOGIN );
        View::set_global('link_logout', Uri::base(false) . Constants::LINK_LOGOUT );
        View::set_global('link_top_cord', Uri::base(false) . Constants::LINK_TOP_CORD);
        View::set_global('link_brand_cord', Uri::base(false) . Constants::LINK_BRAND_CORD);
        View::set_global('link_brand', Uri::base(false) . Constants::LINK_BRAND);
        View::set_global('link_model', Uri::base(false) . Constants::LINK_MODEL);
        View::set_global('link_item', Uri::base(false) . Constants::LINK_ITEM);
        View::set_global('link_category', Uri::base(false) . Constants::LINK_CATEGORY);
        View::set_global('link_user', Uri::base(false) . Constants::LINK_USER);
        View::set_global('link_account', Uri::base(false) . Constants::LINK_ACCOUNT);
        View::set_global('link_import', Uri::base(false) . Constants::LINK_IMPORT);
        View::set_global('link_export', Uri::base(false) . Constants::LINK_EXPORT);

        Config::load('email', 'email');
    }

    public function sendmail($arr)
    {
        $curl = \Request::forge(Config::get('email.defaults.mandrill.api'), 'curl');
        $curl->set_method('post');
        $curl->set_option(CURLOPT_SSL_VERIFYPEER, false);

        $data = array(
            'subject' => $arr['subject'],
            'username' => $this->current_user->username,
            'email' => $this->current_user->email,
            'timestamp' => Date::forge()->format('%m/%d/%Y %H:%M'),
            'data' => $arr['data'],
        );

        $curl->set_params(array(
            'key' => Config::get('email.defaults.mandrill.key'),
            'message' => array(
                'from_email' => Config::get('email.defaults.from.email'),
                'to' => Config::get('email.defaults.mandrill.to'),
                'subject' => Constants::MAIL_SUBJECT_PREFIX.$arr['subject'],
                'html' => View_Twig::forge('mail', $data, false)->render(),
            ),
        ));

        try
        {
            $response = $curl->execute()->response();
        }
        catch (Exception $e)
        {
            var_dump($e->getMessage());
            exit();
        }
    }

    public function sendUserMail($arr)
    {
        $curl = \Request::forge(Config::get('email.defaults.mandrill.api'), 'curl');
        $curl->set_method('post');
        $curl->set_option(CURLOPT_SSL_VERIFYPEER, false);

        $data = array(
            'subject' => $arr['subject'],
            'username' => $this->current_user->username,
            'email' => $this->current_user->email,
            'timestamp' => Date::forge()->format('%m/%d/%Y %H:%M'),
            'data' => $arr['data'],
        );

        $curl->set_params(array(
            'key' => Config::get('email.defaults.mandrill.key'),
            'message' => array(
                'from_email' => Config::get('email.defaults.from.email'),
                'to' => array(
                    array(
                        'email' => $arr['data']['Email'],
                        'name' => $arr['data']['User name'],
                        'type' => 'to',
                    ),
                ),
                'subject' => Constants::MAIL_SUBJECT_PREFIX.$arr['subject'],
                'html' => View_Twig::forge('mail_user', $data, false)->render(),
            ),
        ));

        try
        {
            $response = $curl->execute()->response();
        }
        catch (Exception $e)
        {
            var_dump($e->getMessage());
            exit();
        }
    }

    public function filterUser()
    {
        if ($this->current_user->group != 100)
        {
            Response::redirect('grandtop');
        }
        return;
    }

    public function filterArray($array, $filterArray)
    {
        return Arr::filter_keys($array, $filterArray, true);
    }

    public function parseString2Numeric($array)
    {
        foreach ($array as $key => $item)
        {
            if (is_numeric($item))
            {
                $item = (float) $item;
            }
            $array[$key] = $item;
        }

        return $array;
    }
}