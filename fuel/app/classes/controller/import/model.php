<?php

class Controller_Import_Model extends Controller_BaseHybrid
{
    public function before()
    {
        parent::before();
    }

    public function action_index()
    {
    }

    public function action_create()
    {
    }

    public function action_edit($id = null)
    {
    }

    public function action_delete($id = null)
    {
    }

    public function get_data()
    {
    }

    public function post_data()
    {

        $dataColumn = array(
            'name_ja',
            'name_en',
            'name_kana',
            'img',
            'status',
        );

        $dataArray = array(
            'last_updated_by' => 1,
            'created_at' => time(),
        );

        try
        {
            $inFile = Input::file();
            if (!empty($inFile))
            {
                // Start transaction
                DB::start_transaction();

                // Initialize DB
                DB::delete('models')->execute();
                DB::query('alter table models auto_increment = 1')->execute();


                $files = Input::file();

                $filepath = $files['file']['tmp_name'];
                $excel = \PHPExcel_IOFactory::load($filepath);

                $sheet = $excel->getActiveSheet();

                foreach ($sheet->getRowIterator() as $row)
                {
                    if ($row->getRowIndex() > 3)
                    {
                        foreach ($row->getCellIterator() as $cell)
                        {
                            $colIdx = PHPExcel_Cell::columnIndexFromString($cell->getColumn()) - 3;

                            if ($colIdx > -1)
                            {
                                $dataArray[$dataColumn[$colIdx]] = $cell->getValue();
                            }

                        }
                        if (!empty($dataArray['name_ja']))
                        {
                            DB::insert('models')->set($dataArray)->execute();
                        }
                    }

                }
                // Commit transaction
                DB::commit_transaction();
            }
        }
        catch (Exception $e)
        {
            // 未決のトランザクションクエリをロールバックする
            DB::rollback_transaction();

            throw $e;
        }

        return $this->response(
            array(
                'cord' => 200,
                'message'=>'Completed!!',
                'count' => Model_Model::count(),
                'reloadFlag' => true,
            )
        );
    }

    public function put_data($id = null)
    {
    }

    public function delete_data($id = null)
    {
    }
}