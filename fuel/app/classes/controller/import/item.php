<?php

class Controller_Import_Item extends Controller_BaseHybrid
{
    public function before()
    {
        parent::before();
    }

    public function action_index()
    {
    }

    public function action_create()
    {
    }

    public function action_edit($id = null)
    {
    }

    public function action_delete($id = null)
    {
    }

    public function get_data()
    {
    }

    public function post_data()
    {
        $brand_id = Input::post('brand_id');

        $itemColumn = array(
            'name_ja',
            'name_en',
            'name_cn',
            'name_tw',
            'price_ja',
            'price_en',
            'price_cn',
            'price_tw',
            'url',
            'img',
            'remarks',
            'order',
            'order_sub',
        );


        $dataArray = array(
            'last_updated_by' => 1,
            'created_at' => time(),
        );

        try
        {
            $inFile = Input::file();
            if (!empty($inFile))
            {
                $files = Input::file();

                $filepath = $files['file']['tmp_name'];
                $excel = PHPExcel_IOFactory::load($filepath);

                $sheet = $excel->getSheet(0);
                if ($brand_id && $brand_id != $sheet->getCell( 'A3' )->getValue())
                {
                    return $this->response(
                        array(
                            'cord' => 404,
                            'message'=>'The imoprted file is for different brand.',
                        )
                    );
                }
                $brand_id = (!$brand_id)? $sheet->getCell( 'A3' )->getValue() : $brand_id;

                $dataArray['brand_id'] = $brand_id;

                $itemArray = $dataArray;
                $brandTopArray = $dataArray;

                // Start transaction
                // DB::start_transaction();

                // Initialize DB
                DB::delete('items')->where('brand_id', $brand_id)->execute();
                DB::delete('brand_top')->where('brand_id', $brand_id)->execute();

                foreach ($sheet->getRowIterator() as $row)
                {
                    if ($row->getRowIndex() > 9)
                    {
                        foreach ($row->getCellIterator() as $cell)
                        {
                            $colIdx = PHPExcel_Cell::columnIndexFromString($cell->getColumn()) - 5;

                            switch ($colIdx)
                            {
                                case -4:
                                    $brandTopArray['order'] = $cell->getCalculatedValue();
                                    break;
                                case -3:
                                    break;
                                case -2:
                                    $brandTopArray['model_id'] = $cell->getCalculatedValue();
                                    break;
                                case -1:
                                    $brandTopArray['img'] = $cell->getCalculatedValue();
                                    break;
                                case -0:
                                    $itemArray['name_ja'] = mb_convert_kana($cell->getCalculatedValue(), 'KV');
                                    break;
                                default:
                                    $itemArray[$itemColumn[$colIdx]] = $cell->getCalculatedValue();
                                    break;
                            }
                        }

                        if (!empty($itemArray['name_ja']))
                        {
                            DB::insert('items')->set($itemArray)->execute();
                        }
                        if (!empty($brandTopArray['model_id']))
                        {
                            DB::insert('brand_top')->set($brandTopArray)->execute();
                        }
                    }

                }
                // Commit transaction
                // DB::commit_transaction();
            }
        }
        catch (Exception $e)
        {
            // Rollback
            // DB::rollback_transaction();

            throw $e;
        }

        return $this->response(
            array(
                'cord' => 200,
                'message'=>'Completed!!',
            )
        );
    }

    public function put_data($id = null)
    {
    }

    public function delete_data($id = null)
    {
    }
}