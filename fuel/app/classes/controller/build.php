<?php

class Controller_Build extends Controller_BaseHybrid
{
    public $data = array();

    public function before()
    {
        parent::before();
    }

    public function action_index()
    {
        // For Mac
        putenv('PATH='.getenv('PATH').':/usr/local/bin/');

        // For Windows

        $cmd = 'grunt build --gruntfile ' . FRONTPATH . 'Gruntfile.js';
        exec($cmd . ' 2>&1', $arr);
    }

    public function action_create()
    {
    }
    public function action_edit($id = null)
    {
    }

    public function action_delete($id = null)
    {
    }

    public function get_data()
    {
        try
        {
            // For Mac
            putenv('PATH='.getenv('PATH').':/usr/local/bin/');
            exec('sudo chmod -R 777 '. FRONTPATH);
            $cmd = 'grunt build --gruntfile ' . FRONTPATH . 'Gruntfile.js';

            // For Windows
            // putenv('PATH='.getenv('PATH').';C:\Program Files\nodejs');
            // $cmd = 'runas /savecred /user:administrator c:\Users\Public\grunt.bat';

            exec($cmd . ' 2>&1', $arr);
            $this->response($arr, 200);
        }
        catch(Exception $e)
        {
            $this->response($e, 500);

        }
    }

    public function post_data()
    {
    }

    public function put_data($id = null)
    {
    }

    public function delete_data($id = null)
    {
    }
}