<?php

class Controller_User extends Controller_BaseHybrid
{
    public $data = array();

    public function before()
    {
        parent::before();

        parent::filterUser();

        $this->data['groups'] = Config::get('simpleauth.groups');
        $this->data['users'] = Model_User::find('all', array());
    }

    public function action_index()
    {
        $data = $this->data;

        foreach ($data['users'] as $i => $user) {
            $user->last_login = ($user->last_login)? Date::forge($user->last_login)->format(Constants::TIME_FORMAT) : Constants::DEFAULT_TIMESTAMP;
            $user->created_at = ($user->created_at)? Date::forge($user->created_at)->format(Constants::TIME_FORMAT) : '';
            $user->group_name = $data['groups'][$user->group]['name'];

            $data['users'][$i] = $user;
        }

        $this->template->title = 'USERS | '.Constants::PAGE_NAME;
        $this->template->content = View_Twig::forge('user/index', $data);
    }

    public function action_create()
    {
        $message = '';
        $data = $this->data;
        $item = new stdClass;

        if (Input::method() == 'POST')
        {
            $item->username = Input::post('username');
            $item->email = Input::post('email');
            $item->password = Input::post('password');
            $item->password_2 = Input::post('password_2');
            $item->group = (int) Input::post('group');

            $val = Model_User::validate('create');

            if ($val->run())
            {
                try {
                    Auth::create_user($item->username, $item->password, $item->email, $item->group);

                    // Email notification
                    parent::sendmail(array(
                        'subject' => Constants::MAIL_SUBJECT_CREATE_USER,
                        'data' => array(
                            'User name' => $item->username,
                            'Email' => $item->email,
                            'Group' => $data['groups'][$item->group]['name'],
                        ),
                    ));

                    parent::sendUserMail(array(
                        'subject' => Constants::MAIL_SUBJECT_CREATE_USER_TARGET,
                        'data' => array(
                            'User name' => $item->username,
                            'Email' => $item->email,
                            'Group' => $data['groups'][$item->group]['name'],
                            'Password' => $item->password,
                        ),
                    ));

                    Response::redirect('user');
                }
                catch (Exception $e) {
                    $message = $e->getMessage();
                }
            }
            else
            {
                $item->username = $val->validated('username');
                $item->email = $val->validated('email');
                $item->group = (int) Input::post('group');

                $message = $val->show_errors();
            }
        }

        $data['data'] = $item;
        $data['error'] = $message;
        $this->template->title = "NEW USER | ".Constants::PAGE_NAME;
        $this->template->content = View_Twig::forge('user/create', $data, false);
    }

    public function action_edit($id = null)
    {
        $message = '';
        $data = $this->data;
        $item = new stdClass;

        is_null($id) and Response::redirect('user');

        if ( ! $item = Model_User::find($id))
        {
            Response::redirect('user');
        }

        $val = Model_User::validate_update('edit');

        if ($val->run())
        {
            $item->email = Input::post('email');
            $item->group = (int) Input::post('group');

            try {
                Auth::update_user(array(
                    'email' => $item->email,
                    'group' => $item->group,
                ), $item->username);

                Response::redirect('user');
            }
            catch (Exception $e) {
                $message = $e->getMessage();
           }
        }
        else
        {
            if (Input::method() == 'POST')
            {
                $item->email = $val->validated('email');
                $item->group = (int) Input::post('group');

                $message = $val->show_errors();
            }
        }

        $data['data'] = $item;
        $data['error'] = $message;

        $this->template->title = "USER EDIT | ".Constants::PAGE_NAME;
        $this->template->content = View_Twig::forge('user/edit', $data, false);
    }

    public function action_delete($id = null)
    {
        $message = '';

        is_null($id) and Response::redirect('user');

        try {
            if ($user = Model_User::find($id))
            {
                $user->delete();

                // Email notification
                parent::sendmail(array(
                    'subject' => Constants::MAIL_SUBJECT_DELETE_USER,
                    'data' => array(
                        'User name' => $user->username,
                        'Email' => $user->email,
                        'Group' => $this->data['groups'][$user->group]['name'],
                    ),
                ));

                $message = 'Deleted item #'.$id;
            }
            else
            {
                $message = 'Could not delete item #'.$id;
            }
            Response::redirect('user');
        }
        catch (Exception $e) {
            $message = $e->getMessage();
        }
    }

    // ここからはRESTAPI。使用していないけど、将来必要なら。。未テスト
    public function get_data()
    {
        $data = $this->data;
        $result = array();
        $res = array(
            'count' => 0,
            'items' => array(),
        );

        $result = $this->getData();
        $res['count'] = count($result);

        foreach ($result as $key => $value) {
            Arr::delete($value, array('password','login_hash'));
            $value['group_name'] = $data['groups'][$value['group']]['name'];
            array_push($res['items'], $value);
        }

        $this->response(array('data'=>$res), 200);
    }

    public function post_data()
    {
        $message = '';
        $code = 0;
        $username = Input::post('username');
        $email = Input::post('email');
        $password = Input::post('password');
        $group = (int) Input::post('group');

        try {
            Auth::create_user($username, $password, $email, $group);
            $message = 'success';
            $code = 200;
        }
        catch (Exception $e) {
            $message = $e->getMessage();
            $code = 500;
        }

        $this->response(array('code'=>$code, 'message'=>$message));
    }

    public function put_data($id = null)
    {
    }

    public function delete_data($id = null)
    {
    }

    public function getData()
    {
        $result = array();
        $objArr = array();

        $item = Model_User::find('all', array());

        foreach ($item as $i => $obj)
        {
            $objArr = $obj->to_array();
            array_push($result, $objArr);
        }

        return $result;
    }
}