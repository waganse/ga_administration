<?php

class Controller_Brand extends Controller_BaseHybrid
{
    public $data = array();
    public $status_arr = array('true', 'false');

    public function before()
    {
        parent::before();
        $this->data['users'] = Model_User::find('all', array());
    }

    public function action_index()
    {
        $data = $this->data;

        $data['data'] = Model_Brand::find('all', array(
            'order_by' => array(
                'up_status' => 'asc',
                'global_status' => 'asc',
                'name_ja' => 'asc'
            ),
        ));

        $data['cnt'] = count($data['data']);

        foreach ($data['data'] as $i => $item) {
            $item->global_status_str = $this->status_arr[$item->global_status];
            $item->up_status_str = $this->status_arr[$item->up_status];
            $item->new_status_str = $this->status_arr[$item->new_status];

            $item->update_user = $data['users'][$item->last_updated_by]->username;

            $item->recent_update_class = ($item->updated_at > time() - Constants::UPDATE_THRESHOLD_SEC)? 'new' : '';
            $item->updated_at = ($item->updated_at)? Date::forge($item->updated_at)->format(Constants::TIME_FORMAT) : '';
            $data['data'][$i] = $item;
        }

        $this->template->title = 'BRANDS | '.Constants::PAGE_NAME;
        $this->template->content = View_Twig::forge('brand/index', $data);
    }

    public function action_create()
    {
        !Auth::check() and Response::redirect('admin');

        $message = '';
        $data = $this->data;
        $item = new stdClass;

        if (Input::method() == 'POST')
        {
            $val = Model_Brand::validate('create');

            if ($val->run())
            {
                $item = Model_Brand::forge(array(
                    'name_ja' => Input::post('name_ja'),
                    'name_en' => Input::post('name_en'),
                    'label' => Input::post('label'),
                    'img' => Input::post('img'),
                    'img_sp' => Input::post('img_sp'),
                    'up_status' => Input::post('up_status'),
                    'new_status' => Input::post('new_status'),
                    'global_status' => Input::post('global_status'),
                    'remarks' => Input::post('remarks'),
                    'last_updated_by' => $this->current_user->id,
                ));

                try
                {
                    $item->save();

                    // Email notification
                    parent::sendmail(array(
                        'subject' => Constants::MAIL_SUBJECT_CREATE_BRAND,
                        'data' => array(
                            'Name (JP)' => ($item->name_ja)? $item->name_ja : '-',
                            'Name (EN)' => ($item->name_en)? $item->name_en : '-',
                            'Label' => ($item->label)? $item->label : '-',
                            'Img URL (PC)' => ($item->img)? $item->img : '-',
                            'Img URL (SP)' => ($item->img_sp)? $item->img_sp : '-',
                            'Up status' => (int) $this->status_arr[$item->up_status],
                            'New status' => (int) $this->status_arr[$item->new_status],
                            'Global status' => (int) $this->status_arr[$item->global_status],
                            'Notes' => ($item->remarks)? $item->remarks : '-',
                        ),
                    ));

                    Response::redirect('brand');
                }
                catch(Exception $e)
                {
                    $message = $e->getMessage();
                }
            }
            else
            {
                $item->name_ja = $val->validated('name_ja');
                $item->name_en = $val->validated('name_en');
                $item->label = $val->validated('label');
                $item->img = $val->validated('img');
                $item->img_sp = $val->validated('img_sp');
                $item->up_status = (int) Input::post('up_status');
                $item->new_status = (int) Input::post('new_status');
                $item->global_status = (int) Input::post('global_status');
                $item->remarks = Input::post('remarks');
                $item->last_updated_by = $this->current_user->id;

                $message = $val->show_errors();
            }
        }
        $item->status_arr = $this->status_arr;

        $data['data'] = $item;
        $data['error'] = $message;
        $this->template->title = "NEW BRAND | ".Constants::PAGE_NAME;
        $this->template->content = View_Twig::forge('brand/create', $data, false);
    }

    public function action_edit($id = null)
    {
        !Auth::check() and Response::redirect('admin');

        $message = '';
        $data = $this->data;
        $item = new stdClass;

        is_null($id) and Response::redirect('brand');

        if ( ! $item = Model_Brand::find($id))
        {
            $message = 'Could not find item #'.$id;
            Response::redirect('brand');
        }

        $val = Model_Brand::validate('edit');

        if ($val->run())
        {
            $item->name_ja = Input::post('name_ja');
            $item->name_en = Input::post('name_en');
            $item->label = Input::post('label');
            $item->img = Input::post('img');
            $item->img_sp = Input::post('img_sp');
            $item->up_status = (int) Input::post('up_status');
            $item->new_status = (int) Input::post('new_status');
            $item->global_status = (int) Input::post('global_status');
            $item->remarks = Input::post('remarks');
            $item->last_updated_by = $this->current_user->id;

            try
            {
                $item->save();

                // Email notification
                parent::sendmail(array(
                    'subject' => Constants::MAIL_SUBJECT_EDIT_BRAND,
                    'data' => array(
                        'Name (JP)' => ($item->name_ja)? $item->name_ja : '-',
                        'Name (EN)' => ($item->name_en)? $item->name_en : '-',
                        'Label' => ($item->label)? $item->label : '-',
                        'Img URL (PC)' => ($item->img)? $item->img : '-',
                        'Img URL (SP)' => ($item->img_sp)? $item->img_sp : '-',
                        'Up status' => $this->status_arr[$item->up_status],
                        'New status' => $this->status_arr[$item->new_status],
                        'Global status' => $this->status_arr[$item->global_status],
                        'Notes' => ($item->remarks)? $item->remarks : '-',
                    ),
                ));

                Response::redirect('brand');
            }
            catch (Exception $e)
            {
                $message = $e->getMessage();
            }
        }
        else
        {
            if (Input::method() == 'POST')
            {
                $item->name_ja = $val->validated('name_ja');
                $item->name_en = $val->validated('name_en');
                $item->label = $val->validated('label');
                $item->img = $val->validated('img');
                $item->img_sp = $val->validated('img_sp');
                $item->up_status = (int) Input::post('up_status');
                $item->new_status = (int) Input::post('new_status');
                $item->global_status = (int) Input::post('global_status');
                $item->remarks = $val->validated('remarks');
                $item->last_updated_by = $this->current_user->id;

                $message = $val->show_errors();
            }
        }
        $item->status_arr = $this->status_arr;

        $data['data'] = $item;
        $data['error'] = $message;
        $this->template->title = "BRAND EDIT | ".Constants::PAGE_NAME;
        $this->template->content = View_Twig::forge('brand/edit', $data, false);
    }

    public function action_delete($id = null)
    {
        !Auth::check() and Response::redirect('admin');

        $message = '';

        is_null($id) and Response::redirect('brand');

        try
        {
            $item = Model_Brand::find($id);
            $item->delete();

            // Email notification
            parent::sendmail(array(
                'subject' => Constants::MAIL_SUBJECT_DELETE_BRAND,
                'data' => array(
                    'Name (JP)' => ($item->name_ja)? $item->name_ja : '-',
                    'Name (EN)' => ($item->name_en)? $item->name_en : '-',
                    'Label' => ($item->label)? $item->label : '-',
                    'Img URL (PC)' => ($item->img)? $item->img : '-',
                    'Img URL (SP)' => ($item->img_sp)? $item->img_sp : '-',
                    'Up status' => $this->status_arr[$item->up_status],
                    'New status' => $this->status_arr[$item->new_status],
                    'Global status' => $this->status_arr[$item->global_status],
                    'Notes' => ($item->remarks)? $item->remarks : '-',
                ),
            ));

            Response::redirect('brand');
        }
        catch (Exception $e)
        {
            $message = $e->getMessage();
        }

        Response::redirect('brand');
    }


    // ここからはRESTAPI。使用していないけど、将来必要なら。。未テスト
    public function get_data()
    {
        $data = $this->data;
        $res = array(
            'cnt' => 0,
            'items' => array(),
        );

        $data['data'] = Model_Brand::find('all', array(
            'order_by' => array('global_status' => 'asc', 'name_ja' => 'asc'),
        ));

        $res['cnt'] = count($data['data']);
        foreach ($data['data'] as $i => $item) {

            $item->last_updated_by = $data['users'][$item->last_updated_by]->username;

            $item->global_status = ($item->global_status)? false : true;

            array_push($res['items'], parent::parseString2Numeric($item));
        }

        $this->response($res, 200);
    }

    public function post_data()
    {
        $message = '';
        $code = 0;
        $props = Input::post();

        try
        {
            $new = Model_Brand::forge($props);
            $new->save();

            $message = 'success';
            $code = 200;
        }
        catch (Exception $e)
        {
            $message = $e->getMessage();
            $code = 500;
        }

        $this->response(array('code'=>$code, 'message'=>$message));
    }

    public function put_data($id = null)
    {
        $message = '';
        $code = 0;
        $props = Input::put();
        $model = null;

        try
        {
            $model = Model_Brand::find($id);
            $model->set($props);
            $model->save();

            $message = 'success';
            $code = 200;
        }
        catch (Exception $e)
        {
            $message = $e->getMessage();
            $code = 500;
        }

        $this->response(array('code' => $code, 'message' => $message, 'data' => $model));
    }

    public function delete_data($id = null)
    {
        $message = '';
        $code = 0;

        try
        {
            $model = Model_Brand::find($id);
            $model->delete();

            $message = 'success';
            $code = 200;
        }
        catch (Exception $e)
        {
            $message = $e->getMessage();
            $code = 500;
        }

        $this->response(array('code' => $code, 'message' => $message));
    }
    public function authCheck()
    {
        if (!Security::check_token())
        {
            $this->response($this->msg, 403);
            $this->response->send(true);
            exit();
        }
    }
}