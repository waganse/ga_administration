<?php

class Controller_Account extends Controller_BaseHybrid
{
    public $data = array();

    public function before()
    {
        parent::before();
    }

    public function action_index()
    {
        $message = '';
        $data = $this->data;
        $item = new stdClass;

        if ( ! $item = Model_User::find($this->current_user->id))
        {
            Response::redirect('user');
        }

        $val = Model_User::validate_account('account');

        if ($val->run())
        {
            $item->password = Input::post('password');
            $item->old_password = Input::post('old_password');

            try
            {
                Auth::update_user(array(
                    'password' => $item->password,
                    'old_password' => $item->old_password,
                ), $item->username);

                Response::redirect('user');
            }
            catch (Exception $e)
            {
                $message = $e->getMessage();
            }
        }
        else
        {
            if (Input::method() == 'POST')
            {
                $message = $val->show_errors();
            }
        }

        $data['error'] = $message;

        $this->template->title = "Password change | ".Constants::PAGE_NAME;
        $this->template->content = View_Twig::forge('account/index', $data, false);
    }

    public function get_data()
    {
    }

    public function post_data()
    {
    }

    public function put_data($id = null)
    {
    }

    public function delete_data($id = null)
    {
    }
}