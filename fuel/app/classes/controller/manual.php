<?php

class Controller_Manual extends Controller_BaseNonAuth
{
    public function before()
    {
        parent::before();
    }

    public function action_index()
    {
        $data = array();

        $data['itemReadyFlag'] = (Model_Brand::count() && Model_Model::count())? true : false;
        $this->template->title = 'MANUAL | '.Constants::PAGE_NAME;
        $this->template->content = View_Twig::forge('manual/index', $data);
    }

    public function action_create()
    {
    }

    public function action_edit($id = null)
    {
    }

    public function action_delete($id = null)
    {
    }

    public function get_data()
    {
    }

    public function post_data()
    {
    }

    public function put_data($id = null)
    {
    }

    public function delete_data($id = null)
    {
    }
}