<?php

class Controller_Unsupport extends Controller_Hybrid
{
    public $template = Constants::TEMPLATE_VIEW_ERROR;

    public function before()
    {
        parent::before();
    }

    public function action_index()
    {
        $this->template->css = Asset::css('app.css');
        $this->template->title = "Unsupported Browser | ".Constants::PAGE_NAME;
        $this->template->message = '<p>Unsupported browser.. (use IE10+)</p>';
        $this->template->content = View_Twig::forge('error');
    }

    public function get_data()
    {
    }

    public function post_data()
    {
    }

    public function put_data($id = null)
    {
    }

    public function delete_data($id = null)
    {
    }
}