<?php
class Controller_Export extends Controller_BaseHybrid
{
    public $data = array();
    public $cntModel = 0;
    public $cntBrand = 0;
    public $cntItem = 0;

    public function before()
    {
        parent::before();
        $this->data['users'] = Model_User::find('all', array());
    }

    public function action_index()
    {
        $data = $this->data;

        $data['brandReadyFlag'] = (Model_Brand::count())? true : false;
        $data['modelReadyFlag'] = (Model_Model::count())? true : false;
        $data['itemReadyFlag'] = (Model_Brandtop::count())? true : false;
        $this->template->title = 'DATA EXPORT | '.Constants::PAGE_NAME;
        $this->template->content = View_Twig::forge('export/index', $data);
    }

    public function get_data()
    {
    }

    public function post_data()
    {
    }

    public function put_data($id = null)
    {
    }

    public function delete_data($id = null)
    {
    }

}