<?php

class Controller_Brandtop extends Controller_BaseHybrid
{
    public $data = array();
    public $status_arr = array('true', 'false');

    public function before()
    {
        parent::before();

        $this->data['brand_id'] = (int) Input::get('id');
        $this->data['brands'] = Model_Brand::find('all', array());
        $this->data['models'] = Model_Model::find('all', array(
            'order_by' => array('status' => 'asc', 'name_kana' => 'asc'),
        ));
        $this->data['items'] = Model_Item::find('all', array());
        $this->data['users'] = Model_User::find('all', array());

        foreach ($this->data['brands'] as $j => $brand) {
            if ((int) $brand->id == $this->data['brand_id'])
            {
                $this->data['brand_name'] = $brand->name_ja;
            }
        }
    }

    public function action_index()
    {
        $data = $this->data;

        $tmpItems = new stdClass;

        $data['data'] = Model_Brandtop::find('all', array(
            'where' => array(
                array('brand_id', $data['brand_id'])
            ),
            'order_by' => array('order' => 'asc'),
        ));

        foreach ($data['data'] as $i => $item) {
            // Satatus string
            $item->photo_status_str = $this->status_arr[$item->photo_status];

            // User name
            $item->update_user = $data['users'][$item->last_updated_by]->username;

            // Model name
            $item->model_name = ($data['models'][$item->model_id])? $data['models'][$item->model_id]->name_ja : '';
            $item->model_img = ($data['models'][$item->model_id] && $data['models'][$item->model_id]->img)? $data['models'][$item->model_id]->img : Config::get('base_url').'assets/img/'.Constants::LINK_NOIMAGE;

            // Item name
            $tmpItems = Model_Item::find('all', array(
                'where' => array(
                    'order' => $item->order,
                    'brand_id' => $data['brand_id'],
                ),
                'order_by' => array(
                    'order' => 'asc',
                    'order_sub' => 'asc',
                ),
            ));

            $i = 1;
            foreach ($tmpItems as $key => $val)
            {
                $item['item_name_'.$i] = $val->name_ja;
                $item['item_price_'.$i] = $val->price_ja;
                $item['item_img_'.$i] = $val->img;
                $i ++;
            }

            // Updated at
            $item->recent_update_class = ($item->updated_at > time() - Constants::UPDATE_THRESHOLD_SEC)? 'new' : '';
            $item->updated_at = ($item->updated_at)? Date::forge($item->updated_at)->format(Constants::TIME_FORMAT) : '';
        }
        $this->template->title = 'BRAND COORDINATION | '.Constants::PAGE_NAME;
        $this->template->content = View_Twig::forge('brandtop/index', $data);
    }

    public function action_create()
    {
        !Auth::check() and Response::redirect('admin');

        $message = '';
        $data = $this->data;
        $item = new stdClass;

        $data['items'] = Model_Item::find('all', array(
            'where' => array(
                array('brand_id', $data['brand_id'])
            ),
        ));

        if (Input::method() == 'POST')
        {
            $val = Model_Brandtop::validate('create');

            if ($val->run())
            {
                $item = Model_Brandtop::forge(array(
                    'brand_id' => $data['brand_id'],
                    'model_id' => (int) Input::post('model_id'),
                    'img' => Input::post('img'),
                    'order' => (int) Input::post('order'),
                    'photo_status' => (int) Input::post('photo_status'),
                    'remarks' => Input::post('remarks'),
                    'last_updated_by' => $this->current_user->id,
                ));

                try
                {
                    $item->save();

                    // Email notification
                    parent::sendmail(array(
                        'subject' => Constants::MAIL_SUBJECT_CREATE_BRAND_CORD,
                        'data' => array(
                            'Order' => $item->order,
                            'Brand' => $data['brand_name'],
                            'Model' => Model_Model::find($item->model_id)->name_ja,
                            'Img URL' => ($item->img)? $item->img : '-',
                            'Photo status' => $this->status_arr[$item->photo_status],
                            'Notes' => ($item->remarks)? $item->remarks : '-',
                        ),
                    ));

                    Response::redirect('brandtop?id='.$data['brand_id']);
                }
                catch(Exception $e)
                {
                    $message = $e->getMessage();
                }
            }
            else
            {
                $item->brand_id = $data['brand_id'];
                $item->model_id = (int) Input::post('model_id');
                $item->img = $val->validated('img');
                $item->order = (int) Input::post('order');
                $item->photo_status = (int) Input::post('photo_status');
                $item->remarks = Input::post('remarks');
                $item->last_updated_by = $this->current_user->id;

                $message = $val->show_errors();
            }
        }
        $item->status_arr = $this->status_arr;

        $data['data'] = $item;
        $data['error'] = $message;
        $this->template->title = "NEW COORDINATION | ".Constants::PAGE_NAME;
        $this->template->content = View_Twig::forge('brandtop/create', $data, false);
    }

    public function action_edit($id = null)
    {
        !Auth::check() and Response::redirect('admin');

        $message = '';
        $data = $this->data;
        $item = new stdClass;

        is_null($id) and Response::redirect('brandtop');

        if ( ! $item = Model_Brandtop::find($id))
        {
            Response::redirect('brandtop?id='.$id);
        }

        $val = Model_Brandtop::validate('edit');

        if ($val->run())
        {
            $item->brand_id = $data['brand_id'];
            $item->model_id = (int) Input::post('model_id');
            $item->img = Input::post('img');
            $item->order = (int) Input::post('order');
            $item->photo_status = (int) Input::post('photo_status');
            $item->remarks = Input::post('remarks');
            $item->last_updated_by = $this->current_user->id;

            try
            {
                $item->save();

                // Email notification
                parent::sendmail(array(
                    'subject' => Constants::MAIL_SUBJECT_EDIT_BRAND_CORD,
                    'data' => array(
                        'Order' => $item->order,
                        'Brand' => $data['brand_name'],
                        'Model' => Model_Model::find($item->model_id)->name_ja,
                        'Img URL' => ($item->img)? $item->img : '-',
                        'Photo status' => $this->status_arr[$item->photo_status],
                        'Notes' => ($item->remarks)? $item->remarks : '-',
                    ),
                ));

                Response::redirect('brandtop?id='.$data['brand_id']);
            }
            catch (Exception $e)
            {
                $message = $e->getMessage();
            }
        }
        else
        {
            if (Input::method() == 'POST')
            {
                $item->brand_id = $data['brand_id'];
                $item->model_id = (int) Input::post('model_id');
                $item->img = $val->validated('img');
                $item->order = (int) Input::post('order');
                $item->photo_status = (int) Input::post('photo_status');
                $item->remarks = Input::post('remarks');
                $item->last_updated_by = $this->current_user->id;

                $message = $val->show_errors();
            }
        }
        $item->status_arr = $this->status_arr;

        $data['data'] = $item;
        $data['error'] = $message;
        $this->template->title = "EDIT BRAND COORDINATION | ".Constants::PAGE_NAME;
        $this->template->content = View_Twig::forge('brandtop/edit', $data, false);
    }

    public function action_delete($id = null)
    {
        !Auth::check() and Response::redirect('admin');

        $message = '';
        $data = $this->data;

        is_null($id) and Response::redirect('brandtop');

        try
        {
            $item = Model_Brandtop::find($id);
            $item->delete();

            // Email notification
            parent::sendmail(array(
                'subject' => Constants::MAIL_SUBJECT_DELETE_BRAND_CORD,
                'data' => array(
                    'Order' => $item->order,
                    'Brand' => $data['brand_name'],
                    'Model' => Model_Model::find($item->model_id)->name_ja,
                    'Img URL' => ($item->img)? $item->img : '-',
                    'Photo status' => $this->status_arr[$item->photo_status],
                    'Notes' => ($item->remarks)? $item->remarks : '-',
                ),
            ));

            Response::redirect('brandtop?id='.$data['brand_id']);
        }
        catch (Exception $e)
        {
            $message = $e->getMessage();
        }

        Response::redirect('brandtop');
    }


    // ここからはRESTAPI。使用していないけど、将来必要なら。。未テスト
    public function get_data($id = null)
    {
        $data = $this->data;
        $res = array(
            'cnt' => 0,
            'items' => array(),
        );

        $data['data'] = Model_Brandtop::find('all', array(
            'where' => array(
                array('brand_id', $data['brand_id'])
            ),
            'order_by' => array('order' => 'asc'),
        ));

        $res['cnt'] = count($data['data']);
        $res['brand'] = parent::parseString2Numeric($data['brands'][$data['brand_id']]);

        foreach ($data['data'] as $i => $item)
        {
            $item = parent::parseString2Numeric($item->to_array());

            $item['model'] = parent::parseString2Numeric($data['models'][$item['model_id']]);

            $item['last_updated_by'] = $data['users'][$item['last_updated_by']]->username;
            $item['photo_status'] = ($item['photo_status'])? true : false;

            $item['items'] = array();

            Arr::delete($item, array('brand_id','model_id','deleted_at',));
            array_push($res['items'], $item);
        }

        $this->response($res, 200);
    }

    public function post_data()
    {
        $message = '';
        $code = 0;
        $props = Input::post();

        try
        {
            $new = Model_Brandtop::forge($props);
            $new->save();

            $message = 'success';
            $code = 200;
        }
        catch (Exception $e)
        {
            $message = $e->getMessage();
            $code = 500;
        }

        $this->response(array('code'=>$code, 'message'=>$message));
    }

    public function put_data($id = null)
    {
    }

    public function delete_data($id = null)
    {
    }
}