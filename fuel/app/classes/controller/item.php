<?php

class Controller_Item extends Controller_BaseHybrid
{
    public $data = array();
    public $status_arr = array('sold-out', 'available');
    public $translate_status_arr = array('done', 'in progress');

    public function before()
    {
        parent::before();

        $this->data['translate_filter'] = (is_numeric(Input::get('trans'))) ? true : false;
        $this->data['translate_status'] = ($this->data['translate_filter']) ? array((int) Input::get('trans')) : array(0, 1);

        $this->data['brand_id'] = (int) Input::get('id');
        $this->data['users'] = Model_User::find('all', array());
        $this->data['brands'] = Model_Brand::find('all', array());

        foreach ($this->data['brands'] as $j => $brand) {
            if ((int) $brand->id == $this->data['brand_id'])
            {
                $this->data['brand_name'] = $brand->name_ja;
            }
        }
    }

    public function action_index()
    {
        $data = $this->data;

        $data['data'] = Model_Item::find('all', array(
            'where' => array(
                array('brand_id', $data['brand_id']),
                array('translate_status', 'in', $data['translate_status']),
            ),
            'order_by' => array(
                'order' => 'asc',
                'order_sub' => 'asc',
                'translate_status' => 'asc',
            ),
        ));

        foreach ($data['data'] as $i => $item) {
            $item->img_tooltip = ($item->img)? $item->img : Config::get('base_url').'assets/img/'.Constants::LINK_NOIMAGE;
            $item->status_str = $this->status_arr[$item->sold_status];
            $item->translate_status_str = $this->translate_status_arr[$item->translate_status];

            $item->update_user = $data['users'][$item->last_updated_by]->username;

            $item->recent_update_class = ($item->updated_at > time() - Constants::UPDATE_THRESHOLD_SEC)? 'new' : '';
            $item->updated_at = ($item->updated_at)? Date::forge($item->updated_at)->format(Constants::TIME_FORMAT) : '';

            $data['data'][$i] = $item;
        }

        $this->template->title = 'ITEMS | '.Constants::PAGE_NAME;
        $this->template->content = View_Twig::forge('item/index', $data);
    }

    public function action_create()
    {
        !Auth::check() and Response::redirect('admin');

        $message = '';
        $data = $this->data;
        $item = new stdClass;

        $item->translate_status = 1;
        $item->sold_status = 1;

        if (Input::method() == 'POST')
        {
            $val = Model_Item::validate('create');

            if ($val->run())
            {
                $item = Model_Item::forge(array(
                    'name_ja' => Input::post('name_ja'),
                    'name_en' => Input::post('name_en'),
                    'name_cn' => Input::post('name_cn'),
                    'name_tw' => Input::post('name_tw'),
                    'brand_id' => $data['brand_id'],
                    'order' => Input::post('order'),
                    'order_sub' => Input::post('order_sub'),
                    'price_ja' => (int) Input::post('price_ja'),
                    'price_en' => (int) Input::post('price_en'),
                    'price_cn' => (int) Input::post('price_cn'),
                    'price_tw' => (int) Input::post('price_tw'),
                    'url' => Input::post('url'),
                    'img' => Input::post('img'),
                    'translate_status' => (int) Input::post('translate_status'),
                    'sold_status' => (int) Input::post('sold_status'),
                    'remarks' => Input::post('remarks'),
                    'last_updated_by' => $this->current_user->id,
                ));

                try {
                    $item->save();

                    // Email notification
                    parent::sendmail(array(
                        'subject' => Constants::MAIL_SUBJECT_CREATE_ITEM,
                        'data' => array(
                            'Brand' => $data['brand_name'],
                            'Cordination order' => ($item->order)? $item->order: '-',
                            'Display order' => ($item->order_sub)? $item->order_sub: '-',
                            'Name (JP)' => ($item->name_ja)? $item->name_ja : '-',
                            'Name (EN)' => ($item->name_en)? $item->name_en : '-',
                            'Name (CN)' => ($item->name_cn)? $item->name_cn : '-',
                            'Name (TW)' => ($item->name_tw)? $item->name_tw : '-',
                            'Price (JPY)' => ($item->price_ja)? $item->price_ja : '-',
                            'Price (USD)' => ($item->price_en)? $item->price_en : '-',
                            'Price (CNY)' => ($item->price_cn)? $item->price_cn : '-',
                            'Price (TWY)' => ($item->price_tw)? $item->price_tw : '-',
                            'URL' => ($item->url)? $item->url : '-',
                            'Image URL' => ($item->img)? $item->img : '-',
                            'Translation status' => $this->translate_status_arr[$item->translate_status],
                            'Sold-out status' => $this->status_arr[$item->sold_status],
                            'Notes' => ($item->remarks)? $item->remarks : '-',
                        ),
                    ));

                    Response::redirect('item?id='.$data['brand_id']);
                }
                catch(Exception $e) {
                    $message = $e->getMessage();
                }
            }
            else
            {
                $item->name_ja = $val->validated('name_ja');
                $item->name_en = $val->validated('name_en');
                $item->name_cn = $val->validated('name_cn');
                $item->name_tw = $val->validated('name_tw');
                $item->brand_id = $data['brand_id'];
                $item->order = $val->validated('order');
                $item->order_sub = $val->validated('order_sub');
                $item->price_ja = (int) $val->validated('price_ja');
                $item->price_en = (int) $val->validated('price_en');
                $item->price_cn = (int) $val->validated('price_cn');
                $item->price_tw = (int) $val->validated('price_tw');
                $item->url = $val->validated('url');
                $item->img = $val->validated('img');
                $item->translate_status = (int) Input::post('translate_status');
                $item->sold_status = (int) Input::post('sold_status');
                $item->remarks = Input::post('remarks');
                $item->last_updated_by = $this->current_user->id;

                $message = $val->show_errors();
            }
        }
        $item->status_arr = $this->status_arr;
        $item->translate_status_arr = $this->translate_status_arr;

        $data['data'] = $item;
        $data['error'] = $message;
        $this->template->title = "NEW ITEM | ".Constants::PAGE_NAME;
        $this->template->content = View_Twig::forge('item/create', $data, false);
    }

    public function action_edit($id = null)
    {
        !Auth::check() and Response::redirect('admin');

        $message = '';
        $data = $this->data;
        $item = new stdClass;

        is_null($id) and Response::redirect('item?id='.$data['brand_id']);

        if ( ! $item = Model_Item::find($id))
        {
            $message = 'Could not find item #'.$id;
            Response::redirect('item?id='.$data['brand_id']);
        }

        $val = Model_Item::validate('edit');

        if ($val->run())
        {
            $item->name_ja = Input::post('name_ja');
            $item->name_en = Input::post('name_en');
            $item->name_cn = Input::post('name_cn');
            $item->name_tw = Input::post('name_tw');
            $item->brand_id = $data['brand_id'];
            $item->order = Input::post('order');
            $item->order_sub = Input::post('order_sub');
            $item->price_ja = (int) Input::post('price_ja');
            $item->price_en = (int) Input::post('price_en');
            $item->price_cn = (int) Input::post('price_cn');
            $item->price_tw = (int) Input::post('price_tw');
            $item->url = Input::post('url');
            $item->img = Input::post('img');
            $item->translate_status = (int) Input::post('translate_status');
            $item->sold_status = (int) Input::post('sold_status');
            $item->remarks = Input::post('remarks');
            $item->last_updated_by = $this->current_user->id;

            try {
                $item->save();

                // Email notification
                parent::sendmail(array(
                    'subject' => Constants::MAIL_SUBJECT_EDIT_ITEM,
                    'data' => array(
                        'Brand' => $data['brand_name'],
                        'Cordination order' => ($item->order)? $item->order : '-',
                        'Display order' => ($item->order_sub)? $item->order_sub : '-',
                        'Name (JP)' => ($item->name_ja)? $item->name_ja : '-',
                        'Name (EN)' => ($item->name_en)? $item->name_en : '-',
                        'Name (CN)' => ($item->name_cn)? $item->name_cn : '-',
                        'Name (TW)' => ($item->name_tw)? $item->name_tw : '-',
                        'Price (JPY)' => ($item->price_ja)? $item->price_ja : '-',
                        'Price (USD)' => ($item->price_en)? $item->price_en : '-',
                        'Price (CNY)' => ($item->price_cn)? $item->price_cn : '-',
                        'Price (TWY)' => ($item->price_tw)? $item->price_tw : '-',
                        'URL' => ($item->url)? $item->url : '-',
                        'Image URL' => ($item->img)? $item->img : '-',
                        'Translation status' => $this->translate_status_arr[$item->translate_status],
                        'Sold-out status' => $this->status_arr[$item->sold_status],
                        'Notes' => ($item->remarks)? $item->remarks : '-',
                    ),
                ));

                Response::redirect('item?id='.$data['brand_id']);
            }
            catch (Exception $e) {
                $message = $e->getMessage();
            }
        }

        else
        {
            if (Input::method() == 'POST')
            {
                $item->name_ja = $val->validated('name_ja');
                $item->name_en = $val->validated('name_en');
                $item->name_cn = $val->validated('name_cn');
                $item->name_tw = $val->validated('name_tw');
                $item->brand_id = $data['brand_id'];
                $item->order = $val->validated('order');
                $item->order_sub = $val->validated('order_sub');
                $item->price_ja = (int) $val->validated('price_ja');
                $item->price_en = (int) $val->validated('price_en');
                $item->price_cn = (int) $val->validated('price_cn');
                $item->price_tw = (int) $val->validated('price_tw');
                $item->url = $val->validated('url');
                $item->img = $val->validated('img');
                $item->translate_status = (int) Input::post('translate_status');
                $item->sold_status = (int) Input::post('sold_status');
                $item->remarks = Input::post('remarks');
                $item->last_updated_by = $this->current_user->id;

                $message = $val->show_errors();
            }
        }
        $item->status_arr = $this->status_arr;
        $item->translate_status_arr = $this->translate_status_arr;

        $data['data'] = $item;
        $data['error'] = $message;
        $this->template->title = "EDIT ITEM | ".Constants::PAGE_NAME;
        $this->template->content = View_Twig::forge('item/edit', $data, false);
    }

    public function action_delete($id = null)
    {
        !Auth::check() and Response::redirect('admin');

        $message = '';

        is_null($id) and Response::redirect('item');

        try {
            $item = Model_Item::find($id);
            $item->delete();

            // Email notification
            parent::sendmail(array(
                'subject' => Constants::MAIL_SUBJECT_DELETE_ITEM,
                'data' => array(
                    'Brand' => $data['brand_name'],
                    'Cordination order' => ($item->order)? $item->order : '-',
                    'Display order' => ($item->order_sub)? $item->order_sub : '-',
                    'Name (JP)' => ($item->name_ja)? $item->name_ja : '-',
                    'Name (EN)' => ($item->name_en)? $item->name_en : '-',
                    'Name (CN)' => ($item->name_cn)? $item->name_cn : '-',
                    'Name (TW)' => ($item->name_tw)? $item->name_tw : '-',
                    'Price (JPY)' => ($item->price_ja)? $item->price_ja : '-',
                    'Price (USD)' => ($item->price_en)? $item->price_en : '-',
                    'Price (CNY)' => ($item->price_cn)? $item->price_cn : '-',
                    'Price (TWY)' => ($item->price_tw)? $item->price_tw : '-',
                    'URL' => ($item->url)? $item->url : '-',
                    'Image URL' => ($item->img)? $item->img : '-',
                    'Translation status' => $this->translate_status_arr[$item->translate_status],
                    'Sold-out status' => $this->status_arr[$item->sold_status],
                    'Notes' => ($item->remarks)? $item->remarks : '-',
                ),
            ));

            Response::redirect('item?id='.$this->data['brand_id']);
        }
        catch (Exception $e) {
            $message = $e->getMessage();
        }

        Response::redirect('item?id='.$this->data['brand_id']);
    }

    // ここからはRESTAPI。使用していないけど、将来必要なら。。未テスト
    public function get_data()
    {
        $data = $this->data;
        $res = array(
            'cnt' => 0,
            'items' => array(),
        );

        $data['data'] = Model_Item::find('all', array(
            'where' => array(
                array('brand_id', $data['brand_id']),
                array('translate_status', 'in', $data['translate_status']),
            ),
            'order_by' => array('translate_status' => 'asc'),
        ));

        $res['cnt'] = count($data['data']);
        $res['brand'] = parent::parseString2Numeric($data['brands'][$data['brand_id']]);

        foreach ($data['data'] as $i => $item) {
            $item = $item->to_array();

            $item['last_updated_by'] = $data['users'][$item['last_updated_by']]->username;

            $item['translate_status'] = ($item['translate_status'])? true : false;
            $item['sold_status'] = ($item['sold_status'])? true : false;

            Arr::delete($item, array('brand_id','deleted_at',));
            array_push($res['items'], parent::parseString2Numeric($item));
        }

        $this->response($res, 200);
    }

    public function post_data()
    {
        $message = '';
        $code = 0;
        $props = Input::post();

        try {
            $new = Model_Item::forge($props);
            $new->save();

            $message = 'success';
            $code = 200;
        }
        catch (Exception $e) {
            $message = $e->getMessage();
            $code = 500;
        }

        $this->response(array('code'=>$code, 'message'=>$message));
    }

    public function put_data($id = null)
    {
    }

    public function delete_data($id = null)
    {
    }
}