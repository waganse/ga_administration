<?php
abstract class Controller_BaseNonAuth extends Controller_Hybrid {
    public $template = Constants::TEMPLATE_VIEW;

    public function before()
    {
        parent::before();

        View::set_global('noauth', true);
        View::set_global('css', Asset::css('app.css'));
        View::set_global('js', Asset::js('app.js'));
        View::set_global('svg', Config::get('base_url').'assets/img/svg-symbols.svg');

        View::set_global('link_base', Uri::base(false));
        View::set_global('link_login', Uri::base(false) . Constants::LINK_LOGIN );
        View::set_global('link_logout', Uri::base(false) . Constants::LINK_LOGOUT );
        View::set_global('link_top_cord', Uri::base(false) . Constants::LINK_TOP_CORD);
        View::set_global('link_brand_cord', Uri::base(false) . Constants::LINK_BRAND_CORD);
        View::set_global('link_brand', Uri::base(false) . Constants::LINK_BRAND);
        View::set_global('link_model', Uri::base(false) . Constants::LINK_MODEL);
        View::set_global('link_item', Uri::base(false) . Constants::LINK_ITEM);
        View::set_global('link_category', Uri::base(false) . Constants::LINK_CATEGORY);
        View::set_global('link_user', Uri::base(false) . Constants::LINK_USER);
        View::set_global('link_account', Uri::base(false) . Constants::LINK_ACCOUNT);
        View::set_global('link_import', Uri::base(false) . Constants::LINK_IMPORT);
        View::set_global('link_export', Uri::base(false) . Constants::LINK_EXPORT);
    }
}