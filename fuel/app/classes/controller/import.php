<?php
class Controller_Import extends Controller_BaseHybrid
{
    public $data = array();
    public $cntModel = 0;
    public $cntBrand = 0;
    public $cntItem = 0;

    public function before()
    {
        parent::before();

        parent::filterUser();

        $this->data['cntBrand'] = Model_Brand::count();
        $this->data['cntModel'] = Model_Model::count();
        $this->data['cntItem'] = Model_Item::count();

        $this->data['itemReadyFlag'] = ($this->data['cntBrand'] == 0 || $this->data['cntModel'] == 0)? false : true;

        $this->data['users'] = Model_User::find('all', array());
    }

    public function action_index()
    {
        $data = $this->data;

        $this->template->title = 'DATA IMPORT | '.Constants::PAGE_NAME;
        $this->template->content = View_Twig::forge('import/index', $data);
    }

    public function action_create()
    {
        !Auth::check() and Response::redirect('admin');

        $message = '';
        $data = $this->data;
        $item = new stdClass;

        $item->photo_status = 1;

        if (Input::method() == 'POST')
        {
            $val = Model_Grandtop::validate('create');

            if ($val->run())
            {
                $item = Model_Grandtop::forge(array(
                    'brand_id' => Input::post('brand_id'),
                    'order' => (int) Input::post('order'),
                    'remarks' => Input::post('remarks'),
                    'last_updated_by' => $this->current_user->id,
                ));

                try {
                    $item->save();

                    // Email notification
                    parent::sendmail(array(
                        'subject' => Constants::MAIL_SUBJECT_CREATE_TOP_CORD,
                        'data' => array(
                            'Order' => $item->order,
                            'Brand' => ($item->brand_id)? Model_Brand::find($item->brand_id)->name_ja : '-',
                            'Notes' => ($item->remarks)? $item->remarks : '-',
                        ),
                    ));

                    Response::redirect('grandtop');
                }
                catch(Exception $e) {
                    $message = $e->getMessage();
                }
            }
            else
            {
                $item->brand_id = $val->validated('brand_id');
                $item->order = (int) Input::post('order');
                $item->remarks = Input::post('remarks');
                $item->last_updated_by = $this->current_user->id;

                $message = $val->show_errors();
            }
        }
        $item->status_arr = $this->status_arr;

        $data['items'] = null;
        $data['data'] = $item;
        $data['error'] = $message;
        $this->template->title = "NEW COORDINATION | ".Constants::PAGE_NAME;
        $this->template->content = View_Twig::forge('grandtop/create', $data, false);
    }

    public function action_edit($id = null)
    {
        !Auth::check() and Response::redirect('admin');

        $message = '';
        $data = $this->data;
        $item = new stdClass;

        is_null($id) and Response::redirect('grandtop');

        if ( ! $item = Model_Grandtop::find($id))
        {
            $message = 'Could not find item #'.$id;
            Response::redirect('grandtop');
        }

        $val = Model_Grandtop::validate('edit');

        if ($val->run())
        {
            $item->brand_id = (int) Input::post('brand_id');
            $item->order = (int) Input::post('order');
            $item->remarks = Input::post('remarks');
            $item->last_updated_by = $this->current_user->id;

            try {
                $item->save();

                // Email notification
                parent::sendmail(array(
                    'subject' => Constants::MAIL_SUBJECT_EDIT_TOP_CORD,
                    'data' => array(
                        'Order' => $item->order,
                        'Brand' => ($item->brand_id)? Model_Brand::find($item->brand_id)->name_ja : '-',
                        'Notes' => ($item->remarks)? $item->remarks : '-',
                    ),
                ));

                Response::redirect('grandtop');
            }
            catch (Exception $e) {
                $message = $e->getMessage();
            }
        }
        else
        {
            if (Input::method() == 'POST')
            {
                $item->brand_id = (int) Input::post('brand_id');
                $item->order = (int) Input::post('order');
                $item->remarks = Input::post('remarks');
                $item->last_updated_by = $this->current_user->id;

                $message = $val->show_errors();
            }
        }
        $item->status_arr = $this->status_arr;

        $data['data'] = $item;
        $data['error'] = $message;
        $this->template->title = "EDIT TOP COORDINATION | ".Constants::PAGE_NAME;
        $this->template->content = View_Twig::forge('grandtop/edit', $data, false);
    }

    public function action_delete($id = null)
    {
        !Auth::check() and Response::redirect('admin');

        $message = '';

        is_null($id) and Response::redirect('grandtop');

        try {
            $item = Model_Grandtop::find($id);
            $item->delete();

            // Email notification
            parent::sendmail(array(
                'subject' => Constants::MAIL_SUBJECT_DELETE_TOP_CORD,
                'data' => array(
                    'Order' => $item->order,
                    'Brand' => ($item->brand_id)? Model_Brand::find($item->brand_id)->name_ja : '-',
                    'Notes' => ($item->remarks)? $item->remarks : '-',
                ),
            ));

            Response::redirect('grandtop');
        }
        catch (Exception $e) {
            $message = $e->getMessage();
        }

        Response::redirect('grandtop');
    }

    public function get_data()
    {
        $data = $this->data;
        $res = array(
            'cnt' => 0,
            'items' => array(),
        );

        $data['data'] = Model_Grandtop::find('all', array(
            'order_by' => array('order' => 'asc'),
        ));

        $res['cnt'] = count($data['data']);

        foreach ($data['data'] as $i => $item)
        {
            $item = parent::parseString2Numeric($item->to_array());

            $item['model'] = parent::parseString2Numeric($data['models'][$item['model_id']]);
            $item['brand'] = parent::parseString2Numeric($data['brands'][$item['brand_id']]);

            $item['last_updated_by'] = $data['users'][$item['last_updated_by']]->username;
            $item['photo_status'] = ($item['photo_status'])? true : false;

            $item['items'] = array();
            for ($i = 1; $i <= Constants::MAX_TOP_CORD_ITEM; $i++)
            {
                if ($item['item_'.$i])
                {
                    array_push($item['items'], parent::parseString2Numeric($data['items'][$item['item_'.$i]]));
                }
            }

            Arr::delete($item, array('brand_id','model_id','deleted_at',));
            array_push($res['items'], $item);
        }

        $this->response($res, 200);
    }

    public function post_data()
    {
        $message = '';
        $code = 0;
        $props = Input::post();

        try {
            $new = Model_Grandtop::forge($props);
            $new->save();

            $message = 'success';
            $code = 200;
        }
        catch (Exception $e) {
            $message = $e->getMessage();
            $code = 500;
        }

        $this->response(array('code'=>$code, 'message'=>$message));
    }

    public function put_data($id = null)
    {
    }

    public function delete_data($id = null)
    {
    }

    public function authCheck()
    {
        if (!Security::check_token())
        {
            $this->response($this->msg, 403);
            $this->response->send(true);
            exit();
        }
    }
}