<?php

class Controller_Export_Json extends Controller_BaseHybrid
{
    public function before()
    {
        parent::before();

        $this->data['brands'] = Model_Brand::find('all', array(
            'order_by' => array('name_ja' => 'asc'),
        ));
        $this->data['models'] = Model_Model::find('all', array());
        $this->data['items'] = Model_Item::find('all', array());
        $this->data['users'] = Model_User::find('all', array());

        foreach ($this->data['brands'] as $brand)
        {
            $brand->global_status = !$brand->global_status;
            $brand->up_status = !$brand->up_status;
            $brand->new_status = !$brand->new_status;
        }

        foreach ($this->data['items'] as $item)
        {
            $item->translate_status = !$item->translate_status;
            $item->sold_status = !$item->sold_status;
        }
    }

    public function action_index()
    {
    }

    public function action_create()
    {
    }

    public function action_edit($id = null)
    {
    }

    public function action_delete($id = null)
    {
    }

    public function get_data()
    {
        $result = array();
        $jsonDir = DOCROOT.Constants::JSON_PATH;
        $metaFileName = Constants::META_JSON;
        $mainFileName = Constants::MAIN_JSON;

        try
        {
            $jsonStr = file_get_contents($jsonDir.$metaFileName);

            $result_variables = $this->exportVariables();
            $result_grandtop = $this->exportGrandtop();
            $result_brandtop = $this->exportBrandtop();
            $result_brand = $this->exportBrand();
            $result_model = $this->exportModel();

            $result = Arr::merge($result_variables, $result_grandtop, $result_brandtop, $result_brand, $result_model);

            fopen($jsonDir.$mainFileName, 'w');

            $jsonStr = Format::forge($result)->to_json();
            File::update($jsonDir, $mainFileName, $jsonStr);

            $this->response(array_values(array('data'=>$result)));
        }
        catch(Exception $e)
        {
            $this->response($e, 500);
        }
    }

    public function post_data()
    {
    }

    public function put_data($id = null)
    {
    }

    public function delete_data($id = null)
    {
    }

    public function authCheck()
    {
        if (!Security::check_token())
        {
            $this->response($this->msg, 403);
            $this->response->send(true);
            exit();
        }
    }

    public function exportVariables()
    {
        $result = array(
            'variables' => array(),
        );
        $jsonDir = DOCROOT.Constants::JSON_PATH;
        $fileName = Constants::META_JSON;
        $jsonStr = '';
        $objArr = array();
        $obj = null;

        $obj = json_decode(file_get_contents($jsonDir.$fileName), true);
        $result['variables'] = $obj;

        return $result;
    }

    public function exportGrandtop()
    {
        $result = array(
            'top_cord' => array(),
        );
        $jsonDir = DOCROOT.Constants::JSON_PATH;
        $fileName = Constants::GRANDTOP_JSON;
        $jsonStr = '';
        $objArr = array();
        $jsonKeys = array('order', 'brand', 'model', 'img', 'photo_status', 'up_status', 'new_status', 'global_status', 'items');

        $item = Model_Grandtop::find('all', array(
            'order_by' => array('order' => 'asc'),
        ));

        foreach ($item as $i => $obj)
        {
            $objArr = $obj->to_array();
            $objArr['items'] = array();

            $brandTopArr = Model_Brandtop::find('all', array(
                'where' => array(
                    'brand_id' => $objArr['brand_id'],
                    'order' => 1,
                ),
            ));

            $objArr['brand'] = $this->data['brands'][$objArr['brand_id']];
            $objArr['global_status'] = $objArr['brand']->global_status;
            $objArr['up_status'] = $objArr['brand']->up_status;
            $objArr['new_status'] = $objArr['brand']->new_status;

            foreach ($brandTopArr as $rec)
            {
                $objArr['model'] = $this->data['models'][$rec['model_id']];
                $objArr['photo_status'] = !$rec['photo_status'];
                $objArr['img'] = $rec['img'];
            }

            $objArr['items'] = array();

            $tmpItemArr = Model_Item::find('all', array(
                'where'=> array(
                    'brand_id' => $objArr['brand_id'],
                    'order' => 1,
                ),
                'order_by' => array(
                    'order_sub' => 'asc',
                ),
            ));

            foreach($tmpItemArr as $value)
            {
                array_push($objArr['items'], $value);
            }

            array_push($result['top_cord'], Arr::filter_keys($objArr, $jsonKeys));
        }

        $jsonStr = Format::forge($result)->to_json();

        fopen($jsonDir.$fileName, 'w');
        File::update($jsonDir, $fileName, $jsonStr);

        return $result;
    }

    public function exportBrandtop()
    {
        $data = $this->data;
        $result = array(
            'brand_cord' => array(),
        );
        $jsonDir = DOCROOT.Constants::JSON_PATH;
        $fileName = Constants::BRANDTOP_JSON;
        $jsonStr = '';
        $brandArr = array();
        $itemArr = array();
        $jsonKeys = array('brand', 'model', 'img', 'photo_status', 'global_status', 'items');

        foreach ($data['brands'] as $key => $value) {

            $item = Model_Brandtop::find('all', array(
                'where' => array('brand_id' => $key),
                'order_by' => array('order' => 'asc'),
            ));

            if (!count($item))
            {
                continue;
            }

            $brandArr['brand'] = $data['brands'][$key];
            $brandArr['cordinates'] = array();

            foreach ($item as $i => $obj)
            {
                $itemArr = $obj->to_array();

                $itemArr['model'] = $this->data['models'][$obj['model_id']];
                $itemArr['photo_status'] = !$obj['photo_status'];
                $itemArr['items'] = array();

                $tmpItemArr = Model_Item::find('all', array(
                    'where'=> array(
                        'brand_id' => $key,
                        'order' => $itemArr['order'],
                    ),
                    'order_by' => array(
                        'order_sub' => 'asc',
                    ),
                ));

                foreach($tmpItemArr as $rec)
                {
                    array_push($itemArr['items'], $rec);
                }

                Arr::delete($itemArr, array('brand_id','model_id','last_updated_by','deleted_at',));
                array_push($brandArr['cordinates'], $itemArr);
            }
            array_push($result['brand_cord'], $brandArr);
        }

        $jsonStr = Format::forge($result)->to_json();

        fopen($jsonDir.$fileName, 'w');
        File::update($jsonDir, $fileName, $jsonStr);

        return $result;
    }

    public function exportBrand()
    {
        $result = array(
            'brands' => array(),
        );
        $jsonDir = DOCROOT.Constants::JSON_PATH;
        $fileName = Constants::BRAND_JSON;
        $jsonStr = '';
        $objArr = array();

        $item = Model_Brand::find('all', array(
            'order_by' => array('name_ja' => 'asc'),
        ));

        foreach ($item as $i => $obj)
        {
            $objArr = $obj->to_array();

            array_push($result['brands'], $objArr);
        }

        $jsonStr = Format::forge($result)->to_json();

        fopen($jsonDir.$fileName, 'w');
        File::update($jsonDir, $fileName, $jsonStr);

        return $result;
    }

    public function exportModel()
    {
        $result = array(
            'models' => array(),
        );
        $jsonDir = DOCROOT.Constants::JSON_PATH;
        $fileName = Constants::MODEL_JSON;
        $jsonStr = '';
        $objArr = array();

        $item = Model_Model::find('all', array(
            'where' => array('status' => 0),
            'order_by' => array('name_kana' => 'asc'),
        ));

        foreach ($item as $i => $obj)
        {
            $objArr = $obj->to_array();
            array_push($result['models'], $objArr);
        }

        $jsonStr = Format::forge($result)->to_json();

        fopen($jsonDir.$fileName, 'w');
        File::update($jsonDir, $fileName, $jsonStr);

        return $result;
    }
}