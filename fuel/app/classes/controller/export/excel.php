<?php

class Controller_Export_Excel extends Controller_BaseNonAuth
{
    public $download = array();

    public function before()
    {
        parent::before();

        $this->data['type'] = Input::get('type');
        $this->data['brand_id'] = Input::get('brand');

        $this->data['brands'] = Model_Brand::find('all', array(
            'order_by' => array('name_ja' => 'asc'),
        ));
        $this->data['models'] = Model_Model::find('all', array(
            'order_by' => array('status' => 'asc', 'name_kana' => 'asc'),
        ));
        $this->data['items'] = Model_Item::find('all', array());
        $this->data['users'] = Model_User::find('all', array());
        $this->data['groups'] = Config::get('simpleauth.groups');
        $this->data['top'] = Model_Grandtop::find('all', array(
            'order_by' => array('order'),
        ));
        $this->templateDir = APPPATH.'tmp/';
        $this->exportDir = APPPATH.'exp/';
    }

    public function action_index()
    {
    }

    public function action_create()
    {
    }

    public function action_edit($id = null)
    {
    }

    public function action_delete($id = null)
    {
    }

    public function get_data()
    {
        $result = array();
        $data = $this->data;

        switch ($data['type'])
        {
            case 'top':
                $this->exportGrandtop();
                break;
            case 'brandtop':
                $this->exportBrandtop();
                break;
            case 'brand':
                $this->exportBrand();
                break;
            case 'model':
                $this->exportModel();
                break;
            case 'item':
                $this->exportItem();
                break;
            case 'user':
                $this->exportUser();
                break;
            case 'temp_brand':
                $this->exportTemplateBrand();
                break;
            case 'temp_model':
                $this->exportTemplateModel();
                break;
            case 'temp_coord':
                $this->exportTemplateCoord();
                break;
            default:
                exit();
        }

        $this->exportXls();
    }

    public function post_data()
    {
    }

    public function put_data($id = null)
    {
    }

    public function delete_data($id = null)
    {
    }

    public function exportGrandtop()
    {
        $data = $this->data;

        $rowIdx = 8;
        $rowRange = Constants::MAX_TOP_CORD_ITEM + 1;

        $xls = PHPExcel_IOFactory::load($this->templateDir.Constants::XLS_TEMPLATE_TOP);

        $sheet_ja = $xls->getSheetByName('JP');
        $sheet_en = $xls->getSheetByName('EN');
        $sheet_cn = $xls->getSheetByName('CN');
        $sheet_tw = $xls->getSheetByName('TW');

        $sheet_ja->getProtection()->setSheet(true);
        $sheet_en->getProtection()->setSheet(true);
        $sheet_cn->getProtection()->setSheet(true);
        $sheet_tw->getProtection()->setSheet(true);
        $sheet_ja->getProtection()->setPassword(Constants::XLS_EDIT_PASSWORD);
        $sheet_en->getProtection()->setPassword(Constants::XLS_EDIT_PASSWORD);
        $sheet_cn->getProtection()->setPassword(Constants::XLS_EDIT_PASSWORD);
        $sheet_tw->getProtection()->setPassword(Constants::XLS_EDIT_PASSWORD);


        foreach ($data['top'] as $key => $rec)
        {
            $i = 0;
            $j = 0;
            $rec->brand_name_ja = $data['brands'][$rec->brand_id]->name_ja;
            $rec->brand_name_en = $data['brands'][$rec->brand_id]->name_en;
            $rec->brand_url = $data['brands'][$rec->brand_id]->label;

            $tmpCood = Model_Brandtop::find('all', array(
                'where' => array(
                    'brand_id' => $rec->brand_id,
                    'order' => 1,
                ),
            ));

            $tmpItem = Model_Item::find('all', array(
                'where' => array(
                    'brand_id' => $rec->brand_id,
                    'order' => 1
                ),
                'order_by' => array( 'order_sub' => 'asc' ),
            ));

            $rec->model_name_ja = '';
            $rec->model_name_en = '';
            $rec->model_img = '';
            $rec->img = '';
            $rec->remarks = '';

            foreach ($tmpCood as $cood)
            {
                $rec->model_name_ja = $data['models'][$cood->model_id]->name_ja;
                $rec->model_name_en = $data['models'][$cood->model_id]->name_en;
                $rec->model_img = $data['models'][$cood->model_id]->img;
                $rec->img = $cood->img;
                $rec->remarks = $cood->remarks;
            }

            foreach ($tmpItem as $item)
            {
                $i++;

                $rec['item_'.$i.'_name_ja'] = $item->name_ja;
                $rec['item_'.$i.'_name_en'] = $item->name_en;
                $rec['item_'.$i.'_name_cn'] = $item->name_cn;
                $rec['item_'.$i.'_name_tw'] = $item->name_tw;

                $rec['item_'.$i.'_price_ja'] = $item->price_ja;
                $rec['item_'.$i.'_price_en'] = $item->price_en;
                $rec['item_'.$i.'_price_cn'] = $item->price_cn;
                $rec['item_'.$i.'_price_tw'] = $item->price_tw;

                $rec['item_'.$i.'_url'] = $item->url;
                $rec['item_'.$i.'_img'] = $item->img;
            }

            if ($i < Constants::MAX_TOP_CORD_ITEM)
            {
                for ($j = $i + 1; $j <= Constants::MAX_TOP_CORD_ITEM; $j++ )
                {
                    $rec['item_'.$j.'_name_ja'] = '';
                    $rec['item_'.$j.'_name_en'] = '';
                    $rec['item_'.$j.'_name_cn'] = '';
                    $rec['item_'.$j.'_name_tw'] = '';

                    $rec['item_'.$j.'_price_ja'] = '';
                    $rec['item_'.$j.'_price_en'] = '';
                    $rec['item_'.$j.'_price_cn'] = '';
                    $rec['item_'.$j.'_price_tw'] = '';

                    $rec['item_'.$j.'_url'] = '';
                    $rec['item_'.$j.'_img'] = '';
                }
            }

            $sheet_ja->setCellValueExplicit('A'.$rowIdx, $rec->order, PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet_ja->setCellValueExplicit('B'.$rowIdx, $rec->brand_name_ja, PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet_ja->setCellValueExplicit('C'.$rowIdx, $rec->model_name_ja, PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet_ja->setCellValueExplicit('D'.$rowIdx, $rec->brand_url, PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet_ja->setCellValueExplicit('E'.$rowIdx, $rec->img, PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet_ja->setCellValueExplicit('F'.$rowIdx, $rec->model_img, PHPExcel_Cell_DataType::TYPE_STRING);

            $sheet_en->setCellValueExplicit('A'.$rowIdx, $rec->order, PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet_en->setCellValueExplicit('B'.$rowIdx, $rec->brand_name_en, PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet_en->setCellValueExplicit('C'.$rowIdx, $rec->model_name_en, PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet_en->setCellValueExplicit('D'.$rowIdx, $rec->brand_url, PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet_en->setCellValueExplicit('E'.$rowIdx, $rec->img, PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet_en->setCellValueExplicit('F'.$rowIdx, $rec->model_img, PHPExcel_Cell_DataType::TYPE_STRING);

            $sheet_cn->setCellValueExplicit('A'.$rowIdx, $rec->order, PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet_cn->setCellValueExplicit('B'.$rowIdx, $rec->brand_name_en, PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet_cn->setCellValueExplicit('C'.$rowIdx, $rec->model_name_en, PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet_cn->setCellValueExplicit('D'.$rowIdx, $rec->brand_url, PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet_cn->setCellValueExplicit('E'.$rowIdx, $rec->img, PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet_cn->setCellValueExplicit('F'.$rowIdx, $rec->model_img, PHPExcel_Cell_DataType::TYPE_STRING);

            $sheet_tw->setCellValueExplicit('A'.$rowIdx, $rec->order, PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet_tw->setCellValueExplicit('B'.$rowIdx, $rec->brand_name_en, PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet_tw->setCellValueExplicit('C'.$rowIdx, $rec->model_name_en, PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet_tw->setCellValueExplicit('D'.$rowIdx, $rec->brand_url, PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet_tw->setCellValueExplicit('E'.$rowIdx, $rec->img, PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet_tw->setCellValueExplicit('F'.$rowIdx, $rec->model_img, PHPExcel_Cell_DataType::TYPE_STRING);

            for ( $i = 0; $i < Constants::MAX_TOP_CORD_ITEM; $i++ )
            {
                $sheet_ja->setCellValueExplicit('G'.($rowIdx + $i), $rec['item_'.($i + 1).'_name_ja'], PHPExcel_Cell_DataType::TYPE_STRING);
                $sheet_en->setCellValueExplicit('G'.($rowIdx + $i), $rec['item_'.($i + 1).'_name_en'], PHPExcel_Cell_DataType::TYPE_STRING);
                $sheet_cn->setCellValueExplicit('G'.($rowIdx + $i), $rec['item_'.($i + 1).'_name_cn'], PHPExcel_Cell_DataType::TYPE_STRING);
                $sheet_tw->setCellValueExplicit('G'.($rowIdx + $i), $rec['item_'.($i + 1).'_name_tw'], PHPExcel_Cell_DataType::TYPE_STRING);

                $sheet_ja->setCellValueExplicit('H'.($rowIdx + $i), $rec['item_'.($i + 1).'_price_ja'], PHPExcel_Cell_DataType::TYPE_STRING);
                $sheet_en->setCellValueExplicit('H'.($rowIdx + $i), $rec['item_'.($i + 1).'_price_en'], PHPExcel_Cell_DataType::TYPE_STRING);
                $sheet_cn->setCellValueExplicit('H'.($rowIdx + $i), $rec['item_'.($i + 1).'_price_cn'], PHPExcel_Cell_DataType::TYPE_STRING);
                $sheet_tw->setCellValueExplicit('H'.($rowIdx + $i), $rec['item_'.($i + 1).'_price_tw'], PHPExcel_Cell_DataType::TYPE_STRING);

                $sheet_ja->setCellValueExplicit('I'.($rowIdx + $i), $rec['item_'.($i + 1).'_url'], PHPExcel_Cell_DataType::TYPE_STRING);
                $sheet_en->setCellValueExplicit('I'.($rowIdx + $i), $rec['item_'.($i + 1).'_url'], PHPExcel_Cell_DataType::TYPE_STRING);
                $sheet_cn->setCellValueExplicit('I'.($rowIdx + $i), $rec['item_'.($i + 1).'_url'], PHPExcel_Cell_DataType::TYPE_STRING);
                $sheet_tw->setCellValueExplicit('I'.($rowIdx + $i), $rec['item_'.($i + 1).'_url'], PHPExcel_Cell_DataType::TYPE_STRING);

                $sheet_ja->setCellValueExplicit('J'.($rowIdx + $i), $rec['item_'.($i + 1).'_img'], PHPExcel_Cell_DataType::TYPE_STRING);
                $sheet_en->setCellValueExplicit('J'.($rowIdx + $i), $rec['item_'.($i + 1).'_img'], PHPExcel_Cell_DataType::TYPE_STRING);
                $sheet_cn->setCellValueExplicit('J'.($rowIdx + $i), $rec['item_'.($i + 1).'_img'], PHPExcel_Cell_DataType::TYPE_STRING);
                $sheet_tw->setCellValueExplicit('J'.($rowIdx + $i), $rec['item_'.($i + 1).'_img'], PHPExcel_Cell_DataType::TYPE_STRING);
            }

            $rowIdx += $rowRange;
        }

        $this->download['filename'] = Constants::XLS_TOP;
        $this->download['output'] = $this->exportDir.Constants::XLS_TOP;

        $writer = PHPExcel_IOFactory::createWriter($xls, 'Excel2007');
        $writer->save($this->download['output']);
    }

    public function exportBrandtop()
    {
        $data = $this->data;

        $rowRange = Constants::MAX_BRAND_CORD_ITEM + 1;

        $this->download['filename'] = array();
        $this->download['output'] = array();
        $this->download['zip_filename'] = Constants::ZIP_BRAND_TOP;

        foreach($data['brands'] as $key => $brand)
        {
            $rowIdx = 10;
            $brand_name = $brand->name_ja;

            $template_path = $this->templateDir.Constants::XLS_TEMPLATE_BRAND_TOP;
            $tmp_template_path = $this->templateDir.$brand_name.'_'.Constants::XLS_TEMPLATE_BRAND_TOP;

            // Copy original template to brand unique template
            if (File::exists($tmp_template_path))
            {
                File::delete($tmp_template_path);
            }
            File::copy($template_path, $tmp_template_path);
            $xls = PHPExcel_IOFactory::load($tmp_template_path);

            $sheet = $xls->getSheetByName('COORD');
            $sheet_db = $xls->getSheetByName('DB');

            $sheet->getProtection()->setSheet(true);
            $sheet->getProtection()->setPassword(Constants::XLS_EDIT_PASSWORD);
            $sheet_db->getProtection()->setSheet(true);
            $sheet_db->getProtection()->setPassword(Constants::XLS_EDIT_PASSWORD);

            // DB
            $j = 2;
            foreach($data['models'] as $rec)
            {
                $sheet_db->setCellValueExplicit('A'.($j), $rec->name_ja, PHPExcel_Cell_DataType::TYPE_STRING);
                $sheet_db->setCellValueExplicit('B'.($j), $rec->id, PHPExcel_Cell_DataType::TYPE_STRING);
                $j ++;
            }

            $sheet->setCellValueExplicit('A2', $brand_name, PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->setCellValueExplicit('A3', $key, PHPExcel_Cell_DataType::TYPE_STRING);

            // BRAND TOP
            $data['brand_top'] = Model_Brandtop::find('all', array(
                'where' => array(
                    'brand_id' => $key,
                ),
                'order_by' => array(
                    'order' => 'asc',
                ),
            ));

            $i = 0;
            foreach ($data['brand_top'] as $rec)
            {
                $modelValidation = $sheet->getCell('B'.$rowIdx)->getDataValidation();
                $sheet->setCellValueExplicit('A'.$rowIdx, $rec->order, PHPExcel_Cell_DataType::TYPE_STRING);
                $sheet->setCellValueExplicit('B'.$rowIdx, $data['models'][$rec->model_id]->name_ja, PHPExcel_Cell_DataType::TYPE_STRING);
                $sheet->setCellValueExplicit('D'.$rowIdx, $rec->img, PHPExcel_Cell_DataType::TYPE_STRING);

                // ITEMS
                $data['items'] = Model_Item::find('all', array(
                    'where' => array(
                        'brand_id' => $key,
                        'order' => $rec->order,
                    ),
                    'order_by' => array(
                        'order_sub' => 'asc',
                    ),
                ));

                $j = 0;
                foreach($data['items'] as $item)
                {
                    $sheet->setCellValueExplicit('E'.($rowIdx + $j), $item->name_ja, PHPExcel_Cell_DataType::TYPE_STRING);
                    $sheet->setCellValueExplicit('F'.($rowIdx + $j), $item->name_en, PHPExcel_Cell_DataType::TYPE_STRING);
                    $sheet->setCellValueExplicit('G'.($rowIdx + $j), $item->name_cn, PHPExcel_Cell_DataType::TYPE_STRING);
                    $sheet->setCellValueExplicit('H'.($rowIdx + $j), $item->name_tw, PHPExcel_Cell_DataType::TYPE_STRING);
                    $sheet->setCellValueExplicit('I'.($rowIdx + $j), $item->price_ja, PHPExcel_Cell_DataType::TYPE_STRING);
                    $sheet->setCellValueExplicit('J'.($rowIdx + $j), $item->price_en, PHPExcel_Cell_DataType::TYPE_STRING);
                    $sheet->setCellValueExplicit('K'.($rowIdx + $j), $item->price_cn, PHPExcel_Cell_DataType::TYPE_STRING);
                    $sheet->setCellValueExplicit('L'.($rowIdx + $j), $item->price_tw, PHPExcel_Cell_DataType::TYPE_STRING);
                    $sheet->setCellValueExplicit('M'.($rowIdx + $j), $item->url, PHPExcel_Cell_DataType::TYPE_STRING);
                    $sheet->setCellValueExplicit('N'.($rowIdx + $j), $item->img, PHPExcel_Cell_DataType::TYPE_STRING);
                    $sheet->setCellValueExplicit('O'.($rowIdx + $j), $item->remarks, PHPExcel_Cell_DataType::TYPE_STRING);

                    $j++;
                }

                $modelValidation->setType( PHPExcel_Cell_DataValidation::TYPE_LIST );
                $modelValidation->setAllowBlank(false);
                $modelValidation->setShowInputMessage(true);
                $modelValidation->setShowErrorMessage(true);
                $modelValidation->setShowDropDown(true);
                $modelValidation->setError('Hey, hey, value is not in the list!!');
                $modelValidation->setPrompt('Please pick a value from the drop-down list.');
                $modelValidation->setFormula1('DB!$a$1:$a$200');

                $rowIdx += $rowRange;
                $i ++;
            }

            // Blank cell config
            for ($j = 0; $j < Constants::MAX_BRAND_CORD - $i; $j++)
            {
                $modelValidation = $sheet->getCell('B'.($rowIdx + $rowRange * $j))->getDataValidation();
                $modelValidation->setType( PHPExcel_Cell_DataValidation::TYPE_LIST );
                $modelValidation->setAllowBlank(false);
                $modelValidation->setShowErrorMessage(true);
                $modelValidation->setShowInputMessage(true);
                $modelValidation->setShowDropDown(true);
                $modelValidation->setError('Hey, hey, value is not in the list!!');
                $modelValidation->setPrompt('Please pick a value from the drop-down list.');
                $modelValidation->setFormula1('DB!$a$1:$a$200');
            }

            $output = $this->exportDir.$brand_name.'.xlsx';
            $writer = PHPExcel_IOFactory::createWriter($xls, 'Excel2007');
            $writer->save($output);

            // Delete copied template
            File::delete($tmp_template_path);

            array_push($this->download['filename'], $brand_name.'.xlsx');
            array_push($this->download['output'], $output);
        }
    }

    public function exportBrand()
    {
        $data = $this->data;

        $rowIdx = 4;
        $rowRange = 1;

        $xls = PHPExcel_IOFactory::load($this->templateDir.Constants::XLS_TEMPLATE_BRAND);
        $sheet = $xls->getSheetByName('Brands');

        $sheet->getProtection()->setSheet(true);
        $sheet->getProtection()->setPassword(Constants::XLS_EDIT_PASSWORD);

        $i = 1;
        foreach($data['brands'] as $rec)
        {
            $sheet->setCellValueExplicit('A'.$rowIdx, $i, PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->setCellValueExplicit('B'.$rowIdx, $rec->id, PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->setCellValueExplicit('C'.$rowIdx, $rec->name_ja, PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->setCellValueExplicit('D'.$rowIdx, $rec->name_en, PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->setCellValueExplicit('E'.$rowIdx, $rec->label, PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->setCellValueExplicit('F'.$rowIdx, $rec->img, PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->setCellValueExplicit('G'.$rowIdx, $rec->img_sp, PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->setCellValueExplicit('H'.$rowIdx, $rec->global_status, PHPExcel_Cell_DataType::TYPE_STRING);

            $rowIdx += $rowRange;
            $tmpId = $rec->id;
            $i ++;
        }

        $this->download['filename'] = Constants::XLS_BRAND;
        $this->download['output'] = $this->exportDir.Constants::XLS_BRAND;

        $writer = PHPExcel_IOFactory::createWriter($xls, 'Excel2007');
        $writer->save($this->download['output']);
    }

    public function exportModel()
    {
        $data = $this->data;

        $rowIdx = 4;
        $rowRange = 1;

        $xls = PHPExcel_IOFactory::load($this->templateDir.Constants::XLS_TEMPLATE_MODEL);

        $sheet = $xls->getSheetByName('Models');

        $sheet->getProtection()->setSheet(true);
        $sheet->getProtection()->setPassword(Constants::XLS_EDIT_PASSWORD);

        $i = 1;
        foreach($data['models'] as $rec)
        {
            $sheet->setCellValueExplicit('A'.$rowIdx, $i, PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->setCellValueExplicit('B'.$rowIdx, $rec->id, PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->setCellValueExplicit('C'.$rowIdx, $rec->name_ja, PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->setCellValueExplicit('D'.$rowIdx, $rec->name_en, PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->setCellValueExplicit('E'.$rowIdx, $rec->name_kana, PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->setCellValueExplicit('F'.$rowIdx, $rec->img, PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->setCellValueExplicit('G'.$rowIdx, $rec->status, PHPExcel_Cell_DataType::TYPE_STRING);

            $rowIdx += $rowRange;
            $tmpId = $rec->id;
            $i ++;
        }

        $this->download['filename'] = Constants::XLS_MODEL;
        $this->download['output'] = $this->exportDir.Constants::XLS_MODEL;

        $writer = PHPExcel_IOFactory::createWriter($xls, 'Excel2007');
        $writer->save($this->download['output']);
    }

    public function exportItem()
    {
        $data = $this->data;

        $rowRange = 5;

        $this->download['filename'] = array();
        $this->download['output'] = array();
        $this->download['zip_filename'] = Constants::ZIP_ITEM;

        foreach($data['brands'] as $key => $brand)
        {
            $rowIdx = 4;
            $brand_name = $brand->name_ja;

            $template_path = $this->templateDir.Constants::XLS_TEMPLATE_ITEM;
            $tmp_template_path = $this->templateDir.$brand_name.'_'.Constants::XLS_TEMPLATE_ITEM;

            // Copy original template to brand unique template
            File::copy($template_path, $tmp_template_path);
            $xls = PHPExcel_IOFactory::load($tmp_template_path);

            $sheet = $xls->getSheetByName('Items');

            $sheet->getProtection()->setSheet(true);
            $sheet->getProtection()->setPassword(Constants::XLS_EDIT_PASSWORD);

            $sheet->setCellValueExplicit('A1', $brand_name, PHPExcel_Cell_DataType::TYPE_STRING);

            // Filter items by brand_id
            $data['items'] = Model_Item::find('all', array(
                'where' => array(
                    'brand_id' => $data['brand_id']
                ),
                'order_by' => array(
                    'translate_status' => 'asc',
                ),
            ));

            $i = 1;
            foreach ($data['items'] as $rec)
            {
                $sheet->setCellValueExplicit('A'.$rowIdx, $i, PHPExcel_Cell_DataType::TYPE_STRING);
                $sheet->setCellValueExplicit('B'.$rowIdx, $rec->id, PHPExcel_Cell_DataType::TYPE_STRING);
                $sheet->setCellValueExplicit('D'.$rowIdx, $rec->name_ja, PHPExcel_Cell_DataType::TYPE_STRING);
                $sheet->setCellValueExplicit('D'.($rowIdx + 1), $rec->name_en, PHPExcel_Cell_DataType::TYPE_STRING);
                $sheet->setCellValueExplicit('D'.($rowIdx + 2), $rec->name_cn, PHPExcel_Cell_DataType::TYPE_STRING);
                $sheet->setCellValueExplicit('D'.($rowIdx + 3), $rec->name_tw, PHPExcel_Cell_DataType::TYPE_STRING);
                $sheet->setCellValueExplicit('E'.$rowIdx, $rec->price_ja, PHPExcel_Cell_DataType::TYPE_STRING);
                $sheet->setCellValueExplicit('E'.($rowIdx + 1), $rec->price_en, PHPExcel_Cell_DataType::TYPE_STRING);
                $sheet->setCellValueExplicit('E'.($rowIdx + 2), $rec->price_cn, PHPExcel_Cell_DataType::TYPE_STRING);
                $sheet->setCellValueExplicit('E'.($rowIdx + 3), $rec->price_tw, PHPExcel_Cell_DataType::TYPE_STRING);
                $sheet->setCellValueExplicit('F'.$rowIdx, $rec->img, PHPExcel_Cell_DataType::TYPE_STRING);
                $sheet->setCellValueExplicit('G'.$rowIdx, $rec->url, PHPExcel_Cell_DataType::TYPE_STRING);
                $sheet->setCellValueExplicit('H'.$rowIdx, $rec->translate_status, PHPExcel_Cell_DataType::TYPE_STRING);
                $sheet->setCellValueExplicit('I'.$rowIdx, $rec->sold_status, PHPExcel_Cell_DataType::TYPE_STRING);

                $rowIdx += $rowRange;
                $i ++;
            }

            $output = $this->exportDir.$brand_name.'.xlsx';
            $writer = PHPExcel_IOFactory::createWriter($xls, 'Excel2007');
            $writer->save($output);

            // Delete copied template
            File::delete($tmp_template_path);

            array_push($this->download['filename'], $brand_name.'.xlsx');
            array_push($this->download['output'], $output);
        }
    }

    public function exportUser()
    {
        $data = $this->data;

        $rowIdx = 4;
        $rowRange = 1;

        $xls = PHPExcel_IOFactory::load($this->templateDir.Constants::XLS_TEMPLATE_USER);
        $sheet = $xls->getSheetByName('Users');

        $i = 1;
        foreach($data['users'] as $rec)
        {
            $rec->group_name = $data['groups'][$rec->group]['name'];

            $sheet->setCellValueExplicit('A'.$rowIdx, $i, PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->setCellValueExplicit('B'.$rowIdx, $rec->username, PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->setCellValueExplicit('C'.$rowIdx, $rec->email, PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->setCellValueExplicit('D'.$rowIdx, $rec->group_name, PHPExcel_Cell_DataType::TYPE_STRING);

            $rowIdx += $rowRange;
            $tmpId = $rec->id;
            $i ++;
        }

        $this->download['filename'] = Constants::XLS_USER;
        $this->download['output'] = $this->exportDir.Constants::XLS_USER;

        $writer = PHPExcel_IOFactory::createWriter($xls, 'Excel2007');
        $writer->save($this->download['output']);
    }

    public function exportTemplateBrand()
    {
        $this->download['filename'] = array(
            Constants::XLS_TEMPLATE_BRAND,
        );
        $this->download['output'] = array(
            $this->templateDir.Constants::XLS_TEMPLATE_BRAND,
        );

        $this->download['zip_filename'] = Constants::ZIP_TEMPLATE_BRAND;
    }

    public function exportTemplateModel()
    {
        $this->download['filename'] = array(
            Constants::XLS_TEMPLATE_MODEL,
        );
        $this->download['output'] = array(
            $this->templateDir.Constants::XLS_TEMPLATE_MODEL,
        );

        $this->download['zip_filename'] = Constants::ZIP_TEMPLATE_MODEL;
    }

    public function exportTemplateCoord()
    {
        $data = $this->data;

        $this->download['filename'] = array();
        $this->download['output'] = array();
        $this->download['zip_filename'] = Constants::ZIP_TEMPLATE_COORD;

        $rowIdx = 10;
        $rowRange = Constants::MAX_BRAND_CORD_ITEM + 1;

        foreach($data['brands'] as $key => $brand)
        {
            $brand_name = $brand->name_ja;

            $template_path = $this->templateDir.Constants::XLS_TEMPLATE_BRAND_TOP;
            $tmp_template_path = $this->templateDir.$brand_name.'_'.Constants::XLS_TEMPLATE_BRAND_TOP;

            // Copy original template to brand unique template
            if (File::exists($tmp_template_path))
            {
                File::delete($tmp_template_path);
            }
            File::copy($template_path, $tmp_template_path);
            $xls = PHPExcel_IOFactory::load($tmp_template_path);

            $sheet = $xls->getSheetByName('COORD');
            $sheet_db = $xls->getSheetByName('DB');

            $sheet->getProtection()->setSheet(true);
            $sheet->getProtection()->setPassword(Constants::XLS_EDIT_PASSWORD);
            $sheet_db->getProtection()->setSheet(true);
            $sheet_db->getProtection()->setPassword(Constants::XLS_EDIT_PASSWORD);

            // DB
            $j = 2;
            foreach($data['models'] as $i => $rec)
            {
                $sheet_db->setCellValueExplicit('A'.($j), $rec->name_ja, PHPExcel_Cell_DataType::TYPE_STRING);
                $sheet_db->setCellValueExplicit('B'.($j), $rec->id, PHPExcel_Cell_DataType::TYPE_STRING);
                $j ++;
            }

            $sheet->setCellValueExplicit('A2', $brand_name, PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->setCellValueExplicit('A3', $key, PHPExcel_Cell_DataType::TYPE_STRING);

            for ($j = 0; $j < Constants::MAX_BRAND_CORD; $j++)
            {
                $modelValidation = $sheet->getCell('B'.($rowIdx + $rowRange * $j))->getDataValidation();
                $modelValidation->setType( PHPExcel_Cell_DataValidation::TYPE_LIST );
                $modelValidation->setAllowBlank(false);
                $modelValidation->setShowInputMessage(true);
                $modelValidation->setShowDropDown(true);
                $modelValidation->setPrompt('Please pick a value from the drop-down list.');
                $modelValidation->setFormula1('DB!$a$1:$a$200');
            }

            $output = $this->exportDir.$brand_name.'.xlsx';
            $writer = PHPExcel_IOFactory::createWriter($xls, 'Excel2007');
            $writer->save($output);

            // Delete copied template
            File::delete($tmp_template_path);

            array_push($this->download['filename'], $brand_name.'.xlsx');
            array_push($this->download['output'], $output);
        }
    }

    public function exportXls()
    {
        set_time_limit(0);

        // For multiple xlsx files
        if (gettype($this->download['output']) == 'array')
        {
            $zip = new ZipArchive();
            $zipFileName = $this->download['zip_filename'];
            $zip->open($this->exportDir.$zipFileName, ZIPARCHIVE::CREATE | ZIPARCHIVE::OVERWRITE);

            for ($i = 0; $i < count($this->download['output']); $i++)
            {
                $zip->addFile($this->download['output'][$i], $this->download['filename'][$i]);
            }
            $zip->close();

            foreach (glob($this->exportDir.'*.xlsx') as $val)
            {
                File::delete($val);
            }

            File::download($this->exportDir.$zipFileName, $zipFileName, null, null, true);
        }
        // For single xlsx file
        else
        {
            File::download($this->download['output'], $this->download['filename'], null, null, true);
        }

        return;
    }
}