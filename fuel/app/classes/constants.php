<?php
class Constants
{
    // CONFIG
    const UPDATE_THRESHOLD_SEC = 3600;
    const TIME_FORMAT = '%m/%d/%Y %H:%M';

    // APP
    const PROJECT_PATH = '/labo/fuel_rest/admin_dds/';
    const JSON_PATH = 'front/app/_data/';
    const PAGE_NAME = 'Girls Award Administration';
    const TEMPLATE_VIEW = 'template.twig';
    const TEMPLATE_VIEW_ERROR = 'error.twig';

    // JSON FILE NAME for ECT
    const META_JSON = 'variables.json';
    const MAIN_JSON = 'data.json';
    const GRANDTOP_JSON = 'top_cord.json';
    const BRANDTOP_JSON = 'brand_cord.json';
    const BRAND_JSON = 'brand.json';
    const MODEL_JSON = 'model.json';

    // LINKS
    const LINK_LOGIN = 'admin/login/';
    const LINK_LOGOUT = 'admin/logout/';
    const LINK_TOP_CORD = 'grandtop/';
    const LINK_BRAND_CORD = 'brandtop/';
    const LINK_BRAND = 'brand/';
    const LINK_MODEL = 'model/';
    const LINK_ITEM = 'item/';
    const LINK_CATEGORY = 'category/';
    const LINK_USER = 'user/';
    const LINK_ACCOUNT = 'account/';
    const LINK_IMPORT = 'import/';
    const LINK_EXPORT = 'export/';
    const LINK_NOIMAGE = 'noimage.png';

    // NUMBERS
    const MAX_TOP_CORD_ITEM = 6;
    const MAX_BRAND_CORD = 30;
    const MAX_BRAND_CORD_ITEM = 6;

    // EXCEL/ZIP EXPORT FILE NAME
    const XLS_TEMPLATE_TOP = 'template_top.xlsx';
    const XLS_TOP = 'top_coord.xlsx';

    const XLS_TEMPLATE_BRAND_TOP = 'template_brand_top.xlsx';
    const ZIP_BRAND_TOP = 'brand_cood.zip';

    const XLS_TEMPLATE_BRAND = 'template_brand.xlsx';
    const XLS_BRAND = 'brand.xlsx';

    const XLS_TEMPLATE_MODEL = 'template_model.xlsx';
    const XLS_MODEL = 'model.xlsx';

    const XLS_TEMPLATE_ITEM = 'template_item.xlsx';
    const ZIP_ITEM = 'item.zip';

    const XLS_TEMPLATE_USER = 'template_user.xlsx';
    const XLS_USER = 'user.xlsx';

    const XLS_EDIT_PASSWORD = 'cwd';

    const ZIP_TEMPLATE_BRAND = 'template_brand.zip';
    const ZIP_TEMPLATE_MODEL = 'template_model.zip';
    const ZIP_TEMPLATE_COORD = 'template_coordination.zip';

    // MAIL
    const MAIL_SUBJECT_PREFIX = '[GA 2015 Autumn] ';

    const MAIL_SUBJECT_CREATE_TOP_CORD = 'New Top Coordination';
    const MAIL_SUBJECT_EDIT_TOP_CORD = 'Update on Top Coordination';
    const MAIL_SUBJECT_DELETE_TOP_CORD = 'Top Coordination deleted';

    const MAIL_SUBJECT_CREATE_BRAND_CORD = 'New Brand Coordination';
    const MAIL_SUBJECT_EDIT_BRAND_CORD = 'Update on Brand Coordination';
    const MAIL_SUBJECT_DELETE_BRAND_CORD = 'Brand Coordination deleted';

    const MAIL_SUBJECT_CREATE_BRAND = 'New Brand';
    const MAIL_SUBJECT_EDIT_BRAND = 'Update on Brand';
    const MAIL_SUBJECT_DELETE_BRAND = 'Brand deleted';

    const MAIL_SUBJECT_CREATE_MODEL = 'New Model';
    const MAIL_SUBJECT_EDIT_MODEL = 'Update on Model';
    const MAIL_SUBJECT_DELETE_MODEL = 'Model deleted';

    const MAIL_SUBJECT_CREATE_ITEM = 'New Item';
    const MAIL_SUBJECT_EDIT_ITEM = 'Update on Item';
    const MAIL_SUBJECT_DELETE_ITEM = 'Item deleted';

    const MAIL_SUBJECT_CREATE_USER = 'New User';
    const MAIL_SUBJECT_CREATE_USER_TARGET = 'Your account is now available!';
    const MAIL_SUBJECT_DELETE_USER = 'User deleted';
}
