<!--
<div class="row">
	<div class="col-md-3">
		<?php echo Form::open(array()); ?>

			<?php if (isset($_GET['destination'])): ?>
				<?php echo Form::hidden('destination', $_GET['destination']); ?>
			<?php endif; ?>

			<?php if (isset($login_error)): ?>
				<div class="error"><?php echo $login_error; ?></div>
			<?php endif; ?>

			<div class="form-group <?php echo ! $val->error('email') ?: 'has-error' ?>">
				<label for="email">Email or Username:</label>
				<?php echo Form::input('email', Input::post('email'), array('class' => 'form-control', 'placeholder' => 'Email or Username', 'autofocus')); ?>

				<?php if ($val->error('email')): ?>
					<span class="control-label"><?php echo $val->error('email')->get_message('You must provide a username or email'); ?></span>
				<?php endif; ?>
			</div>

			<div class="form-group <?php echo ! $val->error('password') ?: 'has-error' ?>">
				<label for="password">Password:</label>
				<?php echo Form::password('password', null, array('class' => 'form-control', 'placeholder' => 'Password')); ?>

				<?php if ($val->error('password')): ?>
					<span class="control-label"><?php echo $val->error('password')->get_message(':label cannot be blank'); ?></span>
				<?php endif; ?>
			</div>

			<div class="actions">
				<?php echo Form::submit(array('value'=>'Login', 'name'=>'submit', 'class' => 'btn btn-lg btn-primary btn-block')); ?>
			</div>

		<?php echo Form::close(); ?>
	</div>
</div>
-->

<section class="login">

<?php echo Form::open(array()); ?>
<div class="login__inner">
<div class="login__box">
  <div class="row">
	<div class="col s12">
      <div class="row">
        <div class="input-field col s12">
          <?php echo Form::input('email', Input::post('email'), array('id' => 'username', 'class' => 'validate', 'autofocus')); ?>
          <label for="username">User name or Email</label>
        </div>

		<?php if ($val->error('email')): ?>
			<span class="control-label"><?php echo $val->error('email')->get_message('You must provide a username or email'); ?></span>
		<?php endif; ?>

      </div>

      <div class="row">
        <div class="input-field col s12">
          <?php echo Form::password('password', null, array('id' => 'password', 'class' => 'validate')); ?>
          <label for="password">Password</label>
        </div>

		<?php if ($val->error('password')): ?>
			<span class="control-label"><?php echo $val->error('password')->get_message(':label cannot be blank'); ?></span>
		<?php endif; ?>

      </div>
    </div>
  </div>
</div>
<?php echo Form::button(array('value'=>'Login', 'name'=>'submit', 'class' => 'waves-effect waves-light btn')); ?>
</div>
<?php echo Form::close(); ?>

<div class="link-manual center-align"><a href="<?php echo Config::get('base_url').'manual/'; ?>">Manual</a></div>

<h1>- Girls Award Administration System -</h1>

<div class="row login__message">

<?php if (isset($_GET['destination'])): ?>
	<?php echo Form::hidden('destination', $_GET['destination']); ?>
<?php endif; ?>

<?php if (isset($login_error)): ?>
<div class="card-panel red lighten-3">
	<span class="white-text"><?php echo $login_error; ?></span>
</div>
<?php endif; ?>

</div>
</section>
