// Grunt base configuration
//
// This Gruntfile.js is based on "load-grunt-config" module.
// Config files are separated and put into `grunt/` directory.
// load-grant-config: <https://github.com/firstandthird/load-grunt-config>

'use strict';

module.exports = function (grunt) {

  // Display the execution time.
  require('time-grunt')(grunt);

  // Initialize with "load-grunt-config".
  require('load-grunt-config')(grunt, {

    // Define variables for grunt config here.
    config: {
      path: {
        app:        'app',
        dist:       'dist',
        distDomestic:       'dist/fashion',
        distDomesticSP:       'dist/smart',
        distGlobal:       'dist/global',
        distIgnore: '**/_*/**',     // Excluded directories or files of distribution, e.g. Sass directory.
        preview:       'preview',
        previewDomestic:       'preview/fashion',
        previewGlobal:       'preview/global',
        tmp:        '.tmp/dist',
        tmpPC:        '.tmp/dist/fashion/rba/girlsaward',
        tmpSP:        '.tmp/dist/smart/rba/girlsaward',
        markups:    'app',
        gaMarkupsPC:  'app/fashion/rba/girlsaward',
        gaMarkupsPC_global:  'event/girlsaward',
        gaMarkupsSP:  'app/smart/rba/girlsaward',
        gaMarkupsSP_global:  'event/girlsaward/sp',
        styles:     'app/_stylus',
        scripts:    'app/js',
        images:     'app/img/2015aw',
        sprites:    'app/img/_sprites',
        html:       '.tmp/dist',
        template:       'app/_templates',
        css:        '.tmp/dist/css',
        js:         'app/_js',
        kakunin:    '/default/main/kakunin.rakuten/all/WORKAREA/00-PUBLIC/htdocs/rakuten/swd/ga/2015aw_preview',
        ftpDomestic:    '/default/main/event/all/WORKAREA/00-PUBLIC/htdocs/fashion',
        ftpDomesticSP:    '/default/main/event/all/WORKAREA/00-PUBLIC/htdocs/smart',
        ftpGlobal:    '/default/main/global/all/WORKAREA/00-PUBLIC/htdocs/com/global'
      }
    }

  });

  // Load additional plugins.
  grunt.loadNpmTasks('main-bower-files');

};
