(function($) {
    "use strict";

    var anchor,
        accordion,
        trunk8,
        formatNumber,
        rankToggle,
        nav;

    anchor = function() {
        var $html = $('html, body');

        $('a[data-anchor]').on('click', function(e) {
            e.preventDefault();

            var pos = $($(this).data('anchor')).offset().top;

            $html.animate({
                scrollTop: pos
            }, 500);

            return false;
        });
    };

    nav = function() {
        var $nav = $('ul.page-nav'),
            navPos = $nav.offset().top,
            $body = $('body');
         
            $(window).on('scroll', function() {
                if ($(this).scrollTop() > navPos) {
                    $body.css('margin-top',41);
                    $nav.addClass('fixed');
                } else {
                    $body.css('margin-top',0);
                    $nav.removeClass('fixed');
                }
            });
    };

    accordion = function() {
        var $trigger = $('span.js-accordion-trigger'),
            $overlay = $('#overlay'),
            flg = false,
            $body = $('body'),
            $nav = $('ul.page-nav'),
            wasFixed;

        $trigger.on('click', function() {
            var height = $('body').height();

            if (flg === false) {
                $(this).next('ul.js-accordion-target').addClass('show');
                 if($nav.hasClass('fixed')){
                    $nav.removeClass('fixed');
                    wasFixed = true;
                }
                $overlay.height(height).show();
                $body.addClass('lockscreen');
               
                flg = true;
            }
        });

        $overlay.on('click', function() {
            if (flg === true) {
                $('ul.js-accordion-target.show').removeClass('show');
                $overlay.height('auto').hide();
                $body.removeClass('lockscreen');
                if(wasFixed){
                    $nav.addClass('fixed');
                    wasFixed = false;
                }
                flg = false;
            }
        });
    };

    trunk8 = function() {
        $('.trunk8').trunk8({
            lines: 2
        });
    };

    formatNumber = function(num) {
        return num.toString().replace(/^\d+[^\.]/, function(t){return t.replace(/([\d]+?)(?=(?:\d{3})+$)/g, function(t){ return t + ','; });});
    };

    rankToggle = function() {
        var activeFlag = false;

        $('.js-ranking').on('click', function() {
            var $this = $(this);

            if (!activeFlag) {
                activeFlag = true;
                $('.js-lower-rank').slideToggle(function() {
                    activeFlag = false;
                    $this.find('i').toggleClass('open');
                });
            }
        });
    };

    $(function() {
        anchor();
        nav();
        accordion();
        trunk8();
        // rankToggle();

        $('.js-price-num').each(function() {
            var $this = $(this),
                num = $this.text();

            num = formatNumber(num);
            $this.text(num);
        });
    });

})(jQuery);