(function($) {
    'use strict';

    var allItems = [],
        outer = [],
        knit = [],
        shirt = [],
        cutsew = [],
        onepiece = [],
        bottom = [],
        shoes = [],
        bag = [],
        accessory = [],
        hat = [],
        model = [],
        init,
        updateItemDisplay,
        itemsApiHandler,
        getRandom,
        shuffle,
        formatNumber,
        getArray,
        setArray,
        ua,
        $itemList = $('#recommended-item-list'),
        language = $itemList.data('lang'),
        $error_message = $('div.filter-error-message'),
        $filterBtn = $('li.js-filter'),
        baseURL;
        // tmpURL;

        // if (language === 'ja') {
        //     tmpURL = 'http://event.rakuten.co.jp/fashion/rba/girlsaward/_js/_api/';
        // } else {
        //     tmpURL = 'http://global.rakuten.com/en/event/girlsaward/_api/';
        // }

        if (language === 'ja') {
            baseURL = 'https://app.rakuten.co.jp/services/api/IchibaItem/Search/20140222?format=jsonp&shopCode=stylife&applicationId=girls_award&formatVersion=2&availibility=1&keyword=%e3%80%90GA2015AW';
        } else {
            baseURL = 'https://app.rakuten.co.jp/services/api/IchibaItem/Search/20140222?format=jsonp&shopCode=stylife&applicationId=girls_award&formatVersion=2&availibility=1&keyword=%e3%80%90GA2015AW%08%e3%80%91';
        }

    init = function(allItems) {

        var len = Math.min(allItems.length,40);

        for(var index = 0 ; index < len ; index++){

            var item = allItems[index];

            var modelItem = item['catchcopy'].indexOf('モデル着用') !== -1 ? '<span class="icon-runway">モデル着用</span>' : '';

            var itemName = item['itemName'],
                itemImage = item['mediumImageUrls'][0],
                itemPrice = formatNumber(item['itemPrice']),
                itemUrl = item['itemUrl'],
                itemID = item['itemUrl'].slice(-7,-1);

            if (language === 'ja') {
                $itemList.append(
                    '<li class="col"><div class="item-image"><a href="'+itemUrl+'?l-id=rba_ev_ga20151024_'+ua+'top_item"><img src="'+itemImage+'"></a></div><p class="item-name">'+itemName+'</p><p class="item-price">'+itemPrice+'&#x5186;</p><span class="model-tag">'+modelItem+'</span></li>'
                );
            } else if (language === 'en') {
                $itemList.append(
                    '<li class="col"><div class="item-image"><a href="http://global.rakuten.com/en/store/stylife/item/'+itemID+'/?l-id=rba_ev_ga20151024_'+ua+'en_top_item"><img src="'+itemImage+'"></a></div><p class="item-price">'+itemPrice+'JPY</p><span class="model-tag-en">'+modelItem+'</span></li>'
                );
            } else if (language === 'tw') {
                $itemList.append(
                    '<li class="col"><div class="item-image"><a href="http://global.rakuten.com/zh-tw/store/stylife/item/'+itemID+'/?l-id=rba_ev_ga20151024_'+ua+'tw_top_item"><img src="'+itemImage+'"></a></div><p class="item-price">'+itemPrice+'日元</p><span class="model-tag-tw">'+modelItem+'</span></li>'
                );
            } else {
                $itemList.append(
                    '<li class="col"><div class="item-image"><a href="http://global.rakuten.com/zh-cn/store/stylife/item/'+itemID+'/?l-id=rba_ev_ga20151024_'+ua+'cn_top_item"><img src="'+itemImage+'"></a></div><p class="item-price">'+itemPrice+'日元</p><span class="model-tag-cn">'+modelItem+'</span></li>'
                );
            }
        }

        setTimeout(function() {
            $('p.item-name').trunk8({
                lines: 2
            });
        }, 0);
    };

    itemsApiHandler = function(genre) {
        var _genreId,
            genreId;

        if (typeof(genre) === 'number') {
            _genreId = genre.toString();
            genreId = genre.toString();
        } else {
            _genreId = genre;
            genreId = genre;
        }
        
        var currentArray = [];
        var model = '';

        if (genre === 'model') {
            model = '%20%e3%83%a2%e3%83%87%e3%83%ab%e7%9d%80%e7%94%a8';
            genreId = '';
        }

        currentArray = getArray(_genreId);

            if(typeof currentArray[0] === 'undefined') {

            $.when(
                $.ajax({
                    type: 'GET',
                    dataType: 'jsonp',
                    url: baseURL+'Dickies'+model+'&genreId='+genreId
                }),
                $.ajax({
                    type: 'GET',
                    dataType: 'jsonp',
                    url: baseURL+'EMODA'+model+'&genreId='+genreId
                }),
                $.ajax({
                    type: 'GET',
                    dataType: 'jsonp',
                    url: baseURL+'GUILD%20PRIME'+model+'genreId='+genreId
                }),
                $.ajax({
                    type: 'GET',
                    dataType: 'jsonp',
                    url: baseURL+'GYDA'+model+'&genreId='+genreId
                }),
                $.ajax({
                    type: 'GET',
                    dataType: 'jsonp',
                    url: baseURL+'Heather'+model+'&genreId='+genreId
                }),
                $.ajax({
                    type: 'GET',
                    dataType: 'jsonp',
                    url: baseURL+'JEANASiS'+model+'&genreId='+genreId
                }),
                $.ajax({
                    type: 'GET',
                    dataType: 'jsonp',
                    url: baseURL+'JILL%20by%20JILLSTUART'+model+'&genreId='+genreId
                }),
                $.ajax({
                    type: 'GET',
                    dataType: 'jsonp',
                    url: baseURL+'LOVELESS'+model+'&genreId='+genreId
                }),
                $.ajax({
                    type: 'GET',
                    dataType: 'jsonp',
                    url: baseURL+'LOWRYS%20FARM'+model+'&genreId='+genreId
                }),
                $.ajax({
                    type: 'GET',
                    dataType: 'jsonp',
                    url: baseURL+'Million%20Carats'+model+'&genreId='+genreId
                }),
                $.ajax({
                    type: 'GET',
                    dataType: 'jsonp',
                    url: baseURL+'MURUA'+model+'&genreId='+genreId
                }),
                $.ajax({
                    type: 'GET',
                    dataType: 'jsonp',
                    url: baseURL+'OLIVE%20des%20OLIVE'+model+'&genreId='+genreId
                }),
                $.ajax({
                    type: 'GET',
                    dataType: 'jsonp',
                    url: baseURL+'REDYAZEL'+model+'&genreId='+genreId
                }),
                $.ajax({
                    type: 'GET',
                    dataType: 'jsonp',
                    url: baseURL+'Resexxy'+model+'&genreId='+genreId
                }),
                $.ajax({
                    type: 'GET',
                    dataType: 'jsonp',
                    url: baseURL+'Samantha%20Thavasa'+model+'&genreId='+genreId
                }),
                $.ajax({
                    type: 'GET',
                    dataType: 'jsonp',
                    url: baseURL+'Ungrid'+model+'&genreId='+genreId
                }),
                $.ajax({
                    type: 'GET',
                    dataType: 'jsonp',
                    url: baseURL+'WEGO'+model+'&genreId='+genreId
                })

            ).then(function(brand1, brand2, brand3, brand4, brand5, brand6, brand7, brand8, brand9, brand10, brand11, brand12, brand13, brand14, brand15, brand16, brand17) {

                $.when(
                    $.ajax({
                    type: 'GET',
                    dataType: 'jsonp',
                    url: baseURL+'Dickies'+ model +'&genreId='+ genreId+'&page='+getRandom(brand1[0].pageCount)
                    }),
                    $.ajax({
                    type: 'GET',
                    dataType: 'jsonp',
                    url: baseURL+'EMODA'+ model +'&genreId='+ genreId+'&page='+getRandom(brand2[0].pageCount)
                    }),
                    $.ajax({
                        type: 'GET',
                        dataType: 'jsonp',
                        url: baseURL+'GUILD%20PRIME'+model+'&NGKeyword=lepsim&genreId='+ genreId+'&page='+getRandom(brand3[0].pageCount)
                    }),
                    $.ajax({
                        type: 'GET',
                        dataType: 'jsonp',
                        url: baseURL+'GYDA'+model+'&genreId='+ genreId+'&page='+getRandom(brand4[0].pageCount)
                    }),
                    $.ajax({
                        type: 'GET',
                        dataType: 'jsonp',
                        url: baseURL+'Heather'+model+'&genreId='+ genreId+'&page='+getRandom(brand5[0].pageCount)
                    }),
                    $.ajax({
                        type: 'GET',
                        dataType: 'jsonp',
                        url: baseURL+'JEANASiS'+model+'&genreId='+ genreId+'&page='+getRandom(brand6[0].pageCount)
                    }),
                    $.ajax({
                        type: 'GET',
                        dataType: 'jsonp',
                        url: baseURL+'JILL%20by%20JILLSTUART'+model+'&genreId='+ genreId+'&page='+getRandom(brand7[0].pageCount)
                    }),
                    $.ajax({
                        type: 'GET',
                        dataType: 'jsonp',
                        url: baseURL+'LOVELESS'+model+'&genreId='+ genreId+'&page='+getRandom(brand8[0].pageCount)
                    }),
                    $.ajax({
                        type: 'GET',
                        dataType: 'jsonp',
                        url: baseURL+'LOWRYS%20FARM'+model+'&genreId='+ genreId+'&page='+getRandom(brand9[0].pageCount)
                    }),
                    $.ajax({
                        type: 'GET',
                        dataType: 'jsonp',
                        url: baseURL+'Million%20Carats'+model+'&genreId='+ genreId+'&page='+getRandom(brand10[0].pageCount)
                    }),
                    $.ajax({
                        type: 'GET',
                        dataType: 'jsonp',
                        url: baseURL+'MURUA'+model+'&genreId='+ genreId+'&page='+getRandom(brand11[0].pageCount)
                    }),
                    $.ajax({
                        type: 'GET',
                        dataType: 'jsonp',
                        url: baseURL+'OLIVE%20des%20OLIVE'+model+'&genreId='+ genreId+'&page='+getRandom(brand12[0].pageCount)
                    }),
                    $.ajax({
                        type: 'GET',
                        dataType: 'jsonp',
                        url: baseURL+'REDYAZEL'+model+'&genreId='+ genreId+'&page='+getRandom(brand13[0].pageCount)
                    }),
                    $.ajax({
                        type: 'GET',
                        dataType: 'jsonp',
                        url: baseURL+'Resexxy'+model+'&genreId='+ genreId+'&page='+getRandom(brand14[0].pageCount)
                    }),
                    $.ajax({
                        type: 'GET',
                        dataType: 'jsonp',
                        url: baseURL+'Samantha%20Thavasa'+model+'&genreId='+ genreId+'&page='+getRandom(brand15[0].pageCount)
                    }),
                    $.ajax({
                        type: 'GET',
                        dataType: 'jsonp',
                        url: baseURL+'Ungrid'+model+'&genreId='+ genreId+'&page='+getRandom(brand16[0].pageCount)
                    }),
                    $.ajax({
                        type: 'GET',
                        dataType: 'jsonp',
                        url: baseURL+'WEGO'+model+'&genreId='+ genreId+'&page='+getRandom(brand17[0].pageCount)
                    })


                ).then(function(brand1, brand2, brand3, brand4, brand5, brand6, brand7, brand8, brand9, brand10, brand11, brand12, brand13, brand14, brand15, brand16, brand17){

                    var temp = [].concat(shuffle(brand1[0].Items).slice(0, 5))
                                .concat(shuffle(brand2[0].Items).slice(0,5))
                                .concat(shuffle(brand3[0].Items).slice(0,5))
                                .concat(shuffle(brand4[0].Items).slice(0,5))
                                .concat(shuffle(brand5[0].Items).slice(0,5))
                                .concat(shuffle(brand6[0].Items).slice(0,5))
                                .concat(shuffle(brand7[0].Items).slice(0,5))
                                .concat(shuffle(brand8[0].Items).slice(0,5))
                                .concat(shuffle(brand9[0].Items).slice(0,5))
                                .concat(shuffle(brand10[0].Items).slice(0,5))
                                .concat(shuffle(brand11[0].Items).slice(0,5))
                                .concat(shuffle(brand12[0].Items).slice(0,5))
                                .concat(shuffle(brand13[0].Items).slice(0,5))
                                .concat(shuffle(brand14[0].Items).slice(0,5))
                                .concat(shuffle(brand15[0].Items).slice(0,5))
                                .concat(shuffle(brand16[0].Items).slice(0,5))
                                .concat(shuffle(brand17[0].Items).slice(0,5));

                setArray(_genreId,temp);
                updateItemDisplay(_genreId);

                });
            });

        } else {

            updateItemDisplay(_genreId);

        }
    };

    updateItemDisplay = function (arrayId) {

        var currentArray = getArray(arrayId);
        
        $itemList.empty();

        if (currentArray.length === 0) {
          if (language == 'ja') {
            $error_message.html('<p>表示できる商品がありません。</p>');
          } else {
            $error_message.html('<p>No items</p>');
          }
        } else {
          $error_message.empty();
        }

        var len = Math.min(currentArray.length,40);

        for(var i = 0; i < len; i++){

            var item = currentArray[i];

            var modelItem = item.catchcopy.indexOf('モデル着用') !== -1 ? '<span class="icon-runway">モデル着用</span>' : '';

            var itemName = item['itemName'],
                itemImage = item['mediumImageUrls'][0],
                itemPrice = formatNumber(item['itemPrice']),
                itemShop = item['shopName'],
                itemShopUrl = item['shopUrl'],
                itemUrl = item['itemUrl'],
                itemID = item['itemUrl'].slice(-7,-1);

            if (language === 'ja') {
                $itemList.append(
                    '<li class="col"><div class="item-image"><a href="'+itemUrl+'?l-id=rba_ev_ga20151024_'+ua+'top_item"><img src="'+itemImage+'"></a></div><p class="item-name">'+itemName+'</p><p class="item-price">'+itemPrice+'&#x5186;</p><span class="model-tag">'+modelItem+'</span></li>'
                );
            } else if (language === 'en') {
                $itemList.append(
                    '<li class="col"><div class="item-image"><a href="http://global.rakuten.com/en/store/stylife/item/'+itemID+'/?l-id=rba_ev_ga20151024_'+ua+'en_top_item"><img src="'+itemImage+'"></a></div><p class="item-price">'+itemPrice+'JPY</p><span class="model-tag-en">'+modelItem+'</span></li>'
                );
            } else if (language === 'tw') {
                $itemList.append(
                    '<li class="col"><div class="item-image"><a href="http://global.rakuten.com/zh-tw/store/stylife/item/'+itemID+'/?l-id=rba_ev_ga20151024_'+ua+'tw_top_item"><img src="'+itemImage+'"></a></div><p class="item-price">'+itemPrice+'日元</p><span class="model-tag-tw">'+modelItem+'</span></li>'
                );
            } else {
                $itemList.append(
                    '<li class="col"><div class="item-image"><a href="http://global.rakuten.com/zh-cn/store/stylife/item/'+itemID+'/?l-id=rba_ev_ga20151024_'+ua+'cn_top_item"><img src="'+itemImage+'"></a></div><p class="item-price">'+itemPrice+'日元</p><span class="model-tag-cn">'+modelItem+'</span></li>'
                );
            }
        }

        setTimeout(function() {
            $('p.item-name').trunk8({
                lines: 2
            });
        }, 0);
    };

    getRandom = function(max) {
        var rand = Math.floor(Math.random() * (max - 1)+1);
        return rand > 0? rand : 1;
    };

    shuffle = function(arr) {

        for ( var j, x, i = arr.length - 1; i >= 0; i--) {
            j = Math.floor( Math.random() * (i+1));
            x = arr[i];
            arr[i] = arr[j];
            arr[j] = x;
        }
        return arr;
    };

    formatNumber = function(num) {
        return num.toString().replace(/^\d+[^\.]/, function(t){return t.replace(/([\d]+?)(?=(?:\d{3})+$)/g, function(t){ return t + ','; });});
    };

    getArray = function (arrayId) {

        switch(arrayId){
            case 'all':
                return allItems;
            break;
            case '555087':
                return outer;
            break;
            case '206488':
                return knit;
            break;
            case '206471':
                 return shirt;
            break;
            case '206482':
                return cutsew;
            break;
            case '110729':
                return onepiece;
            break;
            case '555089':
                return bottom;
            break;
            case '100480':
                return shoes;
            break;
            case '110933':
                return bag;
            break;
            case '216129':
                return accessory;
            break;
            case '403732':
                return hat;
            break;
            case 'model':
                return model;
            break;
        }
    };

    setArray = function (arrayId,array) {

        switch(arrayId){
            case 'all':
                 allItems = array;
            break;
            case '555087':
                 outer = array;
            break;
            case '206488':
                 knit = array;
            break;
            case '206471':
                 shirt = array;
            break;
            case '206482':
                 cutsew = array;
            break;
            case '110729':
                 onepiece = array;
            break;
            case '555089':
                 bottom = array;
            break;
            case '100480':
                 shoes = array;
            break;
            case '110933':
                 bag = array;
            break;
            case '216129':
                 accessory = array;
            break;
            case '403732':
                 hat = array;
            break;
            case 'model':
                model = array;
            break;
        }
    };

    $.when(
        $.ajax({
            type: 'GET',
            dataType: 'jsonp',
            url: baseURL+'Dickies'
            // dataType: 'json',
            // url: tmpURL+'dickies.json'
        }),

        $.ajax({
            type: 'GET',
            dataType: 'jsonp',
            url: baseURL+'EMODA'
            // dataType: 'json',
            // url: tmpURL+'emoda.json'
        }),

        $.ajax({
            type: 'GET',
            dataType: 'jsonp',
            url: baseURL+'GUILD%20PRIME'
            // dataType: 'json',
            // url: tmpURL+'guildprime.json'
        }),

        $.ajax({
            type: 'GET',
            dataType: 'jsonp',
            url: baseURL+'GYDA'
            // dataType: 'json',
            // url: tmpURL+'gyda.json'
        }),

        $.ajax({
            type: 'GET',
            dataType: 'jsonp',
            url: baseURL+'Heather'
            // dataType: 'json',
            // url: tmpURL+'heather.json'
        }),

        $.ajax({
            type: 'GET',
            dataType: 'jsonp',
            url: baseURL+'JEANASiS'
            // dataType: 'json',
            // url: tmpURL+'jeanasis.json'
        }),

        $.ajax({
            type: 'GET',
            dataType: 'jsonp',
            url: baseURL+'JILL%20by%20JILLSTUART'
            // dataType: 'json',
            // url: tmpURL+'jill.json'
        }),

        $.ajax({
            type: 'GET',
            dataType: 'jsonp',
            url: baseURL+'LOVELESS'
            // dataType: 'json',
            // url: tmpURL+'loveless.json'
        }),

        $.ajax({
            type: 'GET',
            dataType: 'jsonp',
            url: baseURL+'LOWRYS%20FARM'
            // dataType: 'json',
            // url: tmpURL+'lowrys.json'
        }),

        $.ajax({
            type: 'GET',
            dataType: 'jsonp',
            url: baseURL+'Million%20Carats'
            // dataType: 'json',
            // url: tmpURL+'millioncarats.json'
        }),

        $.ajax({
            type: 'GET',
            dataType: 'jsonp',
            url: baseURL+'MURUA'
            // dataType: 'json',
            // url: tmpURL+'murua.json'
        }),

        $.ajax({
            type: 'GET',
            dataType: 'jsonp',
            url: baseURL+'OLIVE%20des%20OLIVE'
            // dataType: 'json',
            // url: tmpURL+'olive.json'
        }),

        $.ajax({
            type: 'GET',
            dataType: 'jsonp',
            url: baseURL+'REDYAZEL'
            // dataType: 'json',
            // url: tmpURL+'redyazel.json'
        }),

        $.ajax({
            type: 'GET',
            dataType: 'jsonp',
            url: baseURL+'Resexxy'
            // dataType: 'json',
            // url: tmpURL+'resexxy.json'
        }),

        $.ajax({
            type: 'GET',
            dataType: 'jsonp',
            url: baseURL+'Samantha%20Thavasa'
            // dataType: 'json',
            // url: tmpURL+'thavasa.json'
        }),

        $.ajax({
            type: 'GET',
            dataType: 'jsonp',
            url: baseURL+'Ungrid'
            // dataType: 'json',
            // url: tmpURL+'ungrid.json'
        }),

        $.ajax({
            type: 'GET',
            dataType: 'jsonp',
            url: baseURL+'WEGO'
            // dataType: 'json',
            // url: tmpURL+'wego.json'
        })

    ).then(function(brand1, brand2, brand3, brand4, brand5, brand6, brand7, brand8, brand9, brand10, brand11, brand12, brand13, brand14, brand15, brand16, brand17) {
        allItems = [].concat(shuffle(brand1[0].Items).slice(0, 5))
            .concat(shuffle(brand2[0].Items).slice(0, 5))
            .concat(shuffle(brand3[0].Items).slice(0, 5))
            .concat(shuffle(brand4[0].Items).slice(0, 5))
            .concat(shuffle(brand5[0].Items).slice(0, 5))
            .concat(shuffle(brand6[0].Items).slice(0, 5))
            .concat(shuffle(brand7[0].Items).slice(0, 5))
            .concat(shuffle(brand8[0].Items).slice(0, 5))
            .concat(shuffle(brand9[0].Items).slice(0, 5))
            .concat(shuffle(brand10[0].Items).slice(0, 5))
            .concat(shuffle(brand11[0].Items).slice(0, 5))
            .concat(shuffle(brand12[0].Items).slice(0, 5))
            .concat(shuffle(brand13[0].Items).slice(0, 5))
            .concat(shuffle(brand14[0].Items).slice(0, 5))
            .concat(shuffle(brand15[0].Items).slice(0, 5))
            .concat(shuffle(brand16[0].Items).slice(0, 5))
            .concat(shuffle(brand17[0].Items).slice(0, 5));

            init(shuffle(allItems));
        $.when(
            $.ajax({
                type: 'GET',
                dataType: 'jsonp',
                url: baseURL+'Dickies&page='+getRandom(brand1[0].pageCount)
            }),
            $.ajax({
                type: 'GET',
                dataType: 'jsonp',
                url: baseURL+'EMODA&page='+getRandom(brand2[0].pageCount)
            }),
            $.ajax({
                type: 'GET',
                dataType: 'jsonp',
                url: baseURL+'GUILD%20PRIME&page='+getRandom(brand3[0].pageCount)
            }),
            $.ajax({
                type: 'GET',
                dataType: 'jsonp',
                url: baseURL+'GYDA&page='+getRandom(brand4[0].pageCount)
            }),
            $.ajax({
                type: 'GET',
                dataType: 'jsonp',
                url: baseURL+'Heather&page='+getRandom(brand5[0].pageCount)
            }),
            $.ajax({
                type: 'GET',
                dataType: 'jsonp',
                url: baseURL+'JEANASiS&page='+getRandom(brand6[0].pageCount)
            }),
            $.ajax({
                type: 'GET',
                dataType: 'jsonp',
                url: baseURL+'JILL%20by%20JILLSTUART&page='+getRandom(brand7[0].pageCount)
            }),
            $.ajax({
                type: 'GET',
                dataType: 'jsonp',
                url: baseURL+'LOVELESS&page='+getRandom(brand8[0].pageCount)
            }),
            $.ajax({
                type: 'GET',
                dataType: 'jsonp',
                url: baseURL+'LOWRYS%20FARM&NGKeyword=lepsim&page='+getRandom(brand9[0].pageCount)
            }),
            $.ajax({
                type: 'GET',
                dataType: 'jsonp',
                url: baseURL+'Million%20Carats&page='+getRandom(brand10[0].pageCount)
            }),
            $.ajax({
                type: 'GET',
                dataType: 'jsonp',
                url: baseURL+'MURUA&page='+getRandom(brand11[0].pageCount)
            }),
            $.ajax({
                type: 'GET',
                dataType: 'jsonp',
                url: baseURL+'OLIVE%20des%20OLIVE&page='+getRandom(brand12[0].pageCount)
            }),
            $.ajax({
                type: 'GET',
                dataType: 'jsonp',
                url: baseURL+'REDYAZEL&page='+getRandom(brand13[0].pageCount)
            }),
            $.ajax({
                type: 'GET',
                dataType: 'jsonp',
                url: baseURL+'Resexxy&page='+getRandom(brand14[0].pageCount)
            }),
            $.ajax({
                type: 'GET',
                dataType: 'jsonp',
                url: baseURL+'Samantha%20Thavasa&page='+getRandom(brand15[0].pageCount)
            }),
            $.ajax({
                type: 'GET',
                dataType: 'jsonp',
                url: baseURL+'Ungrid&page='+getRandom(brand16[0].pageCount)
            }),
            $.ajax({
                type: 'GET',
                dataType: 'jsonp',
                url: baseURL+'WEGO&page='+getRandom(brand17[0].pageCount)
            })

    ).then(function(brand1, brand2, brand3, brand4, brand5, brand6, brand7, brand8, brand9, brand10, brand11, brand12, brand13, brand14, brand15, brand16, brand17){
            allItems = [].concat(shuffle(brand1[0].Items).slice(0, 5))
                .concat(shuffle(brand2[0].Items).slice(0, 5))
                .concat(shuffle(brand3[0].Items).slice(0, 5))
                .concat(shuffle(brand4[0].Items).slice(0, 5))
                .concat(shuffle(brand5[0].Items).slice(0, 5))
                .concat(shuffle(brand6[0].Items).slice(0, 5))
                .concat(shuffle(brand7[0].Items).slice(0, 5))
                .concat(shuffle(brand8[0].Items).slice(0, 5))
                .concat(shuffle(brand9[0].Items).slice(0, 5))
                .concat(shuffle(brand10[0].Items).slice(0, 5))
                .concat(shuffle(brand11[0].Items).slice(0, 5))
                .concat(shuffle(brand12[0].Items).slice(0, 5))
                .concat(shuffle(brand13[0].Items).slice(0, 5))
                .concat(shuffle(brand14[0].Items).slice(0, 5))
                .concat(shuffle(brand15[0].Items).slice(0, 5))
                .concat(shuffle(brand16[0].Items).slice(0, 5))
                .concat(shuffle(brand17[0].Items).slice(0, 5));
                init(shuffle(allItems));
        });

    }).fail(function() {
      if (language == 'ja') {
        $error_message.html('<p>通信に失敗しました。</p>');
      } else {
        $error_message.html('<p>Connection failed</p>');
      }
    });

    if ((navigator.userAgent.indexOf('iPhone') > 0 && navigator.userAgent.indexOf('iPad') == -1) || navigator.userAgent.indexOf('iPod') > 0 || navigator.userAgent.indexOf('Android') > 0) {
        ua = 'sp_';
    } else {
        ua = '';
    }

    $filterBtn.on('click', function() {
        var $selectedBtn = $(this),
            filterType = $selectedBtn.data('item-type');

        if (filterType === 'all') {
            updateItemDisplay('all');
        } else {
            itemsApiHandler(filterType);
        }

        $filterBtn.removeClass('selected');
        $selectedBtn.addClass('selected');

    });
})(jQuery);