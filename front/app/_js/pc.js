(function($) {
    'use strict';

    var anchor,
        nav,
        carousel,
        rankToggle,
        switchDevice,
        formatNumber,
        tab;

    anchor = function() {
        var $html = $('html, body');

        $('a[data-anchor]').on('click', function(e) {
            e.preventDefault();

            var pos = $($(this).data('anchor')).offset().top;

            $html.animate({
                scrollTop: pos
            }, 500);

            return false;
        });
    };

    nav = function() {
        var $nav = $('.js-nav'),
            navPos = $nav.offset().top;

        $(window).on('scroll', function() {
            if ($(this).scrollTop() > navPos) {
                $nav.addClass('fixed');
            } else {
                $nav.removeClass('fixed');
            }
        });
    };

    carousel = function() {
        var $carouselWrapper = $('.carousel-wrapper');

        $.each($carouselWrapper, function() {
            var $el = $(this),
                $carousel = $el.find('.carousel'),
                $prev = $el.find('.prev'),
                $next = $el.find('.next');

            if ($carousel.find('li').length > 3) {
                $carousel.carouFredSel({
                    items: 1,
                    auto: false,
                    scroll: {
                        items: 1
                    }
                });
                $el.addClass('activated');

                $prev.on('click', function() {
                    $carousel.trigger('prevPage');
                });

                $next.on('click', function() {
                    $carousel.trigger('nextPage');
                });
            }
        });
    };

    rankToggle = function() {
        var activeFlag = false;

        $('.js-ranking').on('click', function() {
            var $this = $(this);

            if (!activeFlag) {
                activeFlag = true;
                $('.js-lower-rank').slideToggle(function() {
                    activeFlag = false;
                    $this.find('i').toggleClass('open');
                });
            }
        });
    };

    switchDevice = function() {
        if (document.referrer.indexOf('event.rakuten.co.jp/fashion/rba/girlsaward/') === -1 &&
            ((navigator.userAgent.indexOf('iPhone') > 0 && navigator.userAgent.indexOf('iPad') === -1) ||
                navigator.userAgent.indexOf('iPod') > 0 ||
                (navigator.userAgent.indexOf('Android') > 0 && navigator.userAgent.indexOf('Mobile') > 0) ||
                navigator.userAgent.indexOf('Windows Phone') > 0 )) {
            if(confirm('Rakuten BRAND AVENUE×GirlsAward公式通販スマートフォン専用サイトへ移動しますか？')) {
                location.href = '/smart/rba/girlsaward/';
            }
        }
    };

    formatNumber = function(num) {
        return num.toString().replace(/^\d+[^\.]/, function(t){return t.replace(/([\d]+?)(?=(?:\d{3})+$)/g, function(t){ return t + ','; });});
    };

    tab = function() {
        var $wrapper = $('.js-tab-wrapper'),
            $tab = $wrapper.find('.js-tab');

        $tab.on('click', function() {
            var $this = $(this);

            $tab.removeClass('js-active');
            $this.addClass('js-active');
        });
    };

    $(function() {
        anchor();
        nav();
        carousel();
        // rankToggle();
        switchDevice();
        tab();

        $('.js-price-num').each(function() {
            var $this = $(this),
                num = $this.text();

            num = formatNumber(num);
            $this.text(num);
        });
    });

})(jQuery);