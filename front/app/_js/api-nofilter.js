(function($) {
    'use strict';

    var allItems = [],
        init,
        getRandom,
        shuffle,
        formatNumber,
        ua,
        $itemList = $('#recommended-item-list'),
        $itemFilter = $('#recommended-filter'),
        brandID = $itemList.data('brand-id'),
        language = $itemList.data('lang'),
        brandName = $('.brand-name').data('brand');

    // get brand api
    var baseURL = 'https://app.rakuten.co.jp/services/api/IchibaItem/Search/20140222?format=jsonp&keyword=%E3%80%90GA2015AW%20'+brandID+'&shopCode=stylife&&applicationId=girls_award&formatVersion=2&NGKeyword=lepsim&availibility=1';

    init = function(allItems) {

        $itemFilter.hide();

        var len = Math.min(allItems.length,60);

        for(var index = 0; index < len; index++){

            var item = allItems[index];

            console.log(item['catchcopy']);

            var modelItem = item['catchcopy'].indexOf('モデル着用') !== -1 ? '<span class="icon-runway">モデル着用</span>' : '';

            // Item data
            var itemName = item['itemName'],
                itemImage = item['mediumImageUrls'][0],
                itemPrice = formatNumber(item['itemPrice']),
                itemUrl = item['itemUrl'],
                itemID = item['itemUrl'].slice(-7,-1);

            if (language === 'ja') {
                $itemList.append(
                    '<li class="col"><div class="item-image"><a href="'+itemUrl+'?l-id=rba_ev_ga20151024_'+ua+'brand_pick_'+brandName+'"><img src="'+itemImage+'"></a></div><p class="item-name">'+itemName+'</p><p class="item-price">'+itemPrice+'&#x5186;</p><span class="model-tag">'+modelItem+'</span></li>'
                );
            } else if (language === 'en') {
                $itemList.append(
                    '<li class="col"><div class="item-image"><a href="http://global.rakuten.com/en/store/stylife/item/'+itemID+'/?l-id=rba_ev_ga20151024_'+ua+'en_brand_pick_'+brandName+'"><img src="'+itemImage+'"></a></div><p class="item-price">'+itemPrice+'JPY</p><span class="model-tag-en">'+modelItem+'</span></li>'
                );
            } else if (language === 'tw') {
                $itemList.append(
                    '<li class="col"><div class="item-image"><a href="http://global.rakuten.com/zh-tw/store/stylife/item/'+itemID+'/?l-id=rba_ev_ga20151024_'+ua+'tw_brand_pick_'+brandName+'"><img src="'+itemImage+'"></a></div><p class="item-price">'+itemPrice+'日元</p><span class="model-tag-tw">'+modelItem+'</span></li>'
                );
            } else {
                $itemList.append(
                    '<li class="col"><div class="item-image"><a href="http://global.rakuten.com/zh-cn/store/stylife/item/'+itemID+'/?l-id=rba_ev_ga20151024_'+ua+'cn_brand_pick_'+brandName+'"><img src="'+itemImage+'"></a></div><p class="item-price">'+itemPrice+'日元</p><span class="model-tag-cn">'+modelItem+'</span></li>'
                );
            }
        }

        setTimeout(function() {
            $('p.item-name').trunk8({
                lines: 2
            });
        }, 0);
    };

    getRandom = function(max) {
        var rand = Math.floor(Math.random() * (max - 1)+1);
        return rand > 0? rand : 1;
    };

    shuffle = function(arr) {
        for ( var j, x, i = arr.length - 1; i >= 0; i--) {
            j = Math.floor( Math.random() * (i+1));
            x = arr [i];
            arr [i] = arr [j];
            arr [j] = x;
        }

        return arr;
    };

    formatNumber = function(num) {
        return num.toString().replace(/^\d+[^\.]/, function(t){return t.replace(/([\d]+?)(?=(?:\d{3})+$)/g, function(t){ return t + ','; });});
    };

    if ((navigator.userAgent.indexOf('iPhone') > 0 && navigator.userAgent.indexOf('iPad') === -1) || navigator.userAgent.indexOf('iPod') > 0 || navigator.userAgent.indexOf('Android') > 0) {
        ua = 'sp_';
    } else {
        ua = '';
    }

    $.when(
        $.ajax({
            type: 'GET',
            dataType: 'jsonp',
            url: baseURL
        })

    ).then(function(brand1) {
        var pageArr = [],
            pageCount = brand1.pageCount;

        if (pageCount < 5) {
            $.when(
                $.ajax({
                    type: 'GET',
                    dataType: 'jsonp',
                    url: baseURL+'&page=1'
                }),
                $.ajax({
                    type: 'GET',
                    dataType: 'jsonp',
                    url: baseURL+'&page=2'
                }),
                $.ajax({
                    type: 'GET',
                    dataType: 'jsonp',
                    url: baseURL+'&page=3'
                }),
                $.ajax({
                    type: 'GET',
                    dataType: 'jsonp',
                    url: baseURL+'&page=4'
                })
            ).then(function(page1, page2, page3){

                allItems = [].concat(shuffle(page1[0].Items).slice(0,10))
                            .concat(shuffle(page2[0].Items).slice(0,10))
                            .concat(shuffle(page3[0].Items).slice(0,10));
                init(shuffle(allItems));
            });
        } else {
            for (var i = 1; i <= pageCount; i++) {
                pageArr.push(i);
                pageArr = _.sample(pageArr, 4);
            }
            $.when(
                $.ajax({
                    type: 'GET',
                    dataType: 'jsonp',
                    url: baseURL+'&page=' + pageArr[0]
                }),
                $.ajax({
                    type: 'GET',
                    dataType: 'jsonp',
                    url: baseURL+'&page=' + pageArr[1]
                }),
                $.ajax({
                    type: 'GET',
                    dataType: 'jsonp',
                    url: baseURL+'&page=' + pageArr[2]
                }),
                $.ajax({
                    type: 'GET',
                    dataType: 'jsonp',
                    url: baseURL+'&page=' + pageArr[3]
                })
            ).then(function(page1, page2, page3){

                allItems = [].concat(shuffle(page1[0].Items).slice(0,10))
                            .concat(shuffle(page2[0].Items).slice(0,10))
                            .concat(shuffle(page3[0].Items).slice(0,10));
                init(shuffle(allItems));
            });
        }

    }).fail(function() {
        if (language == 'ja') {
            $('.filter-error-message').html('<p>通信に失敗しました。</p>');
        } else {
            $('.filter-error-message').html('<p>Connection failed</p>');
        }
    });

})(jQuery);