// Split FIle
//
// grunt-split-file: <https://github.com/kurohara/grunt-split-file>

'use strict';

module.exports = {

  splitfile: {
    options: {
      separator: 'SPLITSPLITSPLIT',
      prefix: [ "1", "2" ]
    },
    files: {
      '.tmp/dist/fashion/rba/girlsaward/': '.tmp/dist/fashion/rba/girlsaward/brand.html'
    },
  }

};
