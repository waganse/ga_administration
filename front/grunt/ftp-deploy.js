// Deploy over FTP
//
// grunt-ftp-deploy: <https://github.com/zonak/grunt-ftp-deploy>

'use strict';

module.exports = {

  preview: {
    auth: {
      host: 'teamsite01.rakuten.co.jp',
      port: 21,
      authKey: 'kakunin'
    },
    src: '<%= path.preview %>',
    dest: '<%= path.kakunin %>',
    exclusions: [
      '<%= path.preview %>/**/img',
      '<%= path.preview %>/.git*',
      '<%= path.preview %>/**/.DS_Store',
      '<%= path.preview %>/**/Thumbs.db'
    ]
  },

  domestic: {
    auth: {
      host: 'teamsite01.rakuten.co.jp',
      port: 21,
      authKey: 'kakunin'
    },
    src: '<%= path.distDomestic %>',
    dest: '<%= path.ftpDomestic %>',
    exclusions: [
      '<%= path.distDomestic %>/**/img',
      '<%= path.distDomestic %>/**/js',
      '<%= path.distDomestic %>/**/css',
      '<%= path.distDomestic %>/.git*',
      '<%= path.distDomestic %>/**/.DS_Store',
      '<%= path.distDomestic %>/**/Thumbs.db'
    ]
  },

  domesticSP: {
    auth: {
      host: 'teamsite01.rakuten.co.jp',
      port: 21,
      authKey: 'kakunin'
    },
    src: '<%= path.distDomesticSP %>',
    dest: '<%= path.ftpDomesticSP %>',
    exclusions: [
      '<%= path.distDomesticSP %>/**/img',
      '<%= path.distDomesticSP %>/**/js',
      '<%= path.distDomesticSP %>/**/css',
      '<%= path.distDomesticSP %>/.git*',
      '<%= path.distDomesticSP %>/**/.DS_Store',
      '<%= path.distDomesticSP %>/**/Thumbs.db'
    ]
  },

  global: {
    auth: {
      host: 'teamsite01.rakuten.co.jp',
      port: 21,
      authKey: 'kakunin'
    },
    src: '<%= path.distGlobal %>',
    dest: '<%= path.ftpGlobal %>',
    exclusions: [
      '<%= path.distGlobal %>/**/img',
      '<%= path.distGlobal %>/**/js',
      '<%= path.distGlobal %>/**/css',
      '<%= path.distGlobal %>/.git*',
      '<%= path.distGlobal %>/**/.DS_Store',
      '<%= path.distGlobal %>/**/Thumbs.db'
    ]
  }

};
