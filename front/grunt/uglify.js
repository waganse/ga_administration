'use strict';

module.exports = {

    dist: {
        files: [{
            expand: true,
            cwd: '<%= path.tmp %>',
            src: ['**/*.js', '!**/*.min.js'],
            dest: '<%= path.tmp %>'
        }]
    }

};
