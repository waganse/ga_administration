// ECT Templates
// grunt-ect: <https://bitbucket.org/2no/grunt-ect>

'use strict';

var grunt = require('grunt'),
    _ = require('underscore'),
    languages = [
        {
            abbr: 'ja',
            folder: ''
        },
        {
            abbr: 'en',
            folder: 'en'
        },
        {
            abbr: 'cn',
            folder: 'zh-cn'
        },
        {
            abbr: 'tw',
            folder: 'zh-tw'
        }
    ],
    data = grunt.file.readJSON('app/_data/data.json'),
    brands = grunt.file.readJSON('app/_data/brand.json'),
    config = grunt.file.readJSON('app/_data/variables.json'),
    indexFileName = (config.config === 'dev')? 'piyo.html' : 'index.html',
    brandName = '',
    brandLabel = '',
    // For site index page
    options = {
        index: {
            options: {
                root: '<%= path.markups %>',
                variables: {
                    languages: languages,
                    data: data,
                    indexFileName: indexFileName
                }
            },
            expand: true,
            cwd: '<%= path.markups %>',
            files: {
                '<%= path.tmp %>/index.html': 'index.ect'
            }
        },
        usemin: {
            options: {
                root: '<%= path.markups %>',
                variables: {
                    languages: languages,
                    data: data
                }
            },
            expand: true,
            cwd: '<%= path.markups %>',
            files: {
                '<%= path.tmp %>/_usemin.html': 'usemin.ect'
            }
        }
    };

    // For brand cordination
    _.each(languages, function(language, i) {
        _.each(brands.brands, function(brand) {
            var brandId = brand.id,
                brandName = brand.name_ja.toLowerCase(),
                brandLabel = brand.label,
                taskNamePC = language.abbr + brandLabel + 'pc',
                taskNameSP = language.abbr + brandLabel + 'sp',
                opt = {};

            // PC
            opt[taskNamePC] = {
                options: {
                    root: '<%= path.gaMarkupsPC %>',
                    variables: {
                        brandId: brandId,
                        brandName: brandName,
                        brandLabel: brandLabel,
                        language: language.abbr,
                        data: data
                    }
                },
                expand: true,
                cwd: '<%= path.gaMarkupsPC %>'
            };

            opt[taskNamePC]['files'] = {};
            if (language.abbr === 'ja') {
                opt[taskNamePC]['files']['<%= path.tmpPC %>/' + brandLabel + '/' + indexFileName] = 'brand.ect';
            } else {
                opt[taskNamePC]['files']['<%= path.tmp %>/global/' + language.folder + '/<%= path.gaMarkupsPC_global %>/' + brandLabel + '/' + indexFileName] = 'brand_global.ect';
            }

            // SP
            opt[taskNameSP] = {
                options: {
                    root: '<%= path.gaMarkupsSP %>',
                    // Define global variables here or read JSON file.
                    variables: {
                        brandId: brandId,
                        brandName: brandName,
                        brandLabel: brandLabel,
                        language: language.abbr,
                        data: data
                    }
                },
                expand: true,
                cwd: '<%= path.gaMarkupsSP %>'
            };

            opt[taskNameSP]['files'] = {};
            if (language.abbr === 'ja') {
                opt[taskNameSP]['files']['<%= path.tmpSP %>/' + brandLabel + '/' + indexFileName] = 'brand.ect';
            } else {
                opt[taskNameSP]['files']['<%= path.tmp %>/global/'+ language.folder +'/<%= path.gaMarkupsSP_global %>/' + brandLabel + '/' + indexFileName] = 'brand_global.ect';
            }

            _.extend(options, opt);
        });

    });

    // For top cordination
    _.each(languages, function(language, i) {
        var taskNamePC = language.abbr + 'pc',
            taskNameSP = language.abbr + 'sp',
            opt = {};

        // PC
        opt[taskNamePC] = {
            options: {
                root: '<%= path.gaMarkupsPC %>',
                variables: {
                    language: language.abbr,
                    data: data
                }
            },
            expand: true,
            cwd: '<%= path.gaMarkupsPC %>'
        };

        opt[taskNamePC]['files'] = {};
        if (language.abbr === 'ja') {
            opt[taskNamePC]['files']['<%= path.tmpPC %>/' + indexFileName] = 'index.ect';
        } else {
            opt[taskNamePC]['files']['<%= path.tmp %>/global/' + language.folder + '/<%= path.gaMarkupsPC_global %>/' + indexFileName] = 'index_global.ect';
        }

        // SP
        opt[taskNameSP] = {
            options: {
                root: '<%= path.gaMarkupsSP %>',
                // Define global variables here or read JSON file.
                variables: {
                    language: language.abbr,
                    data: data
                }
            },
            expand: true,
            cwd: '<%= path.gaMarkupsSP %>'
        };

        opt[taskNameSP]['files'] = {};
        if (language.abbr === 'ja') {
            opt[taskNameSP]['files']['<%= path.tmpSP %>/' + indexFileName] = 'index.ect';
        } else {
            opt[taskNameSP]['files']['<%= path.tmp %>/global/' + language.folder + '/<%= path.gaMarkupsSP_global %>/' + indexFileName] = 'index_global.ect';
        }

        _.extend(options, opt);
    });

module.exports = options;