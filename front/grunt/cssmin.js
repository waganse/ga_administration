'use strict';

module.exports = {

    options: {
        compatibility: 'ie8'
    },

    dist: {
        files: [{
            expand: true,
            cwd: '<%= path.tmp %>',
            src: ['**/*.css', '!**/*.min.css'],
            dest: '<%= path.tmp %>'
        }]
    }

};
