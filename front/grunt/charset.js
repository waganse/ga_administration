// Charset conversion from UTF-8 to other
// grunt-charset:

'use strict';

module.exports = {

    dist_pc: {
      options: {
        from: 'UTF-8',
        to: 'EUC-JP',
        html5: {
          ext: ['.html', '.shtml']
        }
      },
      files: [{
        expand: true,
        cwd: '<%= path.distDomestic %>',
        dest: '<%= path.distDomestic %>',
        src: [
          '**/*.html',
          '**/*.css'
        ]
      }]
    },

    dist_sp: {
      options: {
        from: 'UTF-8',
        to: 'EUC-JP',
        html5: {
          ext: ['.html', '.shtml']
        }
      },
      files: [{
        expand: true,
        cwd: '<%= path.distDomesticSP %>',
        dest: '<%= path.distDomesticSP %>',
        src: [
          '**/*.html',
          '**/*.css'
        ]
      }]
    }

};
