// grunt-kakunin-deploy: <https://git.rakuten-it.com/users/shinsuke.a.yamada/repos/grunt-kakunin-deploy/browse>

'use strict';

module.exports = {

    preview: {
        authKey: 'kakunin',
        subject: 'preveiw',
        dest: '<%= path.kakunin %>'
    },

    domestic: {
        authKey: 'kakunin',
        subject: 'dist domestic',
        dest: '<%= path.kakunin %>'
    },

    global: {
        authKey: 'kakunin',
        subject: 'dist global',
        dest: '<%= path.kakunin %>'
    }

};
