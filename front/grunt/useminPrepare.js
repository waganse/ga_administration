// Prepare for grunt-usemin task
//
// This module requires "usemin" task.
// grunt-usemin: <https://github.com/yeoman/grunt-usemin>

'use strict';

var _ = require('underscore');

module.exports = {

  options: {
    root: [
      '<%= path.tmp %>',
      '<%= path.app %>',
      '.'
    ],
    dest: '<%= path.tmp %>',
    flow: {
      steps: {
        css: ['concat'],
        js: ['concat']
      },
      post: {
          css: [{
              name: 'concat',
              createConfig: function(context, block) {
                  var generated = context.options.generated;

                  _.each(generated.files, function(item) {
                      item.dest = item.dest.replace(/\.tmp\/concat/, '.tmp/dist');
                      console.log(item.dest);
                  });
              }
          }],
      }
    }
  },

  // Parse settings from target HTML code
  html: ['<%= path.tmp %>/**/_usemin.html']

};
