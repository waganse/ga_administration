// Text replace for built html & css

'use strict';

module.exports = {

    htmlDomestic: {
        files: [{
            expand: true,
            cwd: '<%= path.distDomestic %>/',
            src: '**/*.html',
            dest: '<%= path.distDomestic %>/'
        }],
        options: {
            replacements: [
                {
                    pattern: /href="(\/fashion\/rba\/girlsaward\/css\/)/g,
                    replacement: 'href="http://event.rakuten.co.jp$1'
                },
                {
                    pattern: /src="(\/fashion\/rba\/girlsaward\/js\/)/g,
                    replacement: 'src="http://event.rakuten.co.jp$1'
                },
            ]
        }
    },

    htmlDomesticSP: {
        files: [{
            expand: true,
            cwd: '<%= path.distDomesticSP %>/',
            src: '**/*.html',
            dest: '<%= path.distDomesticSP %>/'
        }],
        options: {
            replacements: [
                {
                    pattern: /href="(\/smart\/rba\/girlsaward\/css\/)/g,
                    replacement: 'href="http://event.rakuten.co.jp$1'
                },
                {
                    pattern: /src="(\/smart\/rba\/girlsaward\/js\/)/g,
                    replacement: 'src="http://event.rakuten.co.jp$1'
                }
            ]
        }
    },

    htmlGlobal: {
        files: [{
            expand: true,
            cwd: '<%= path.distGlobal %>/',
            src: '**/*.html',
            dest: '<%= path.distGlobal %>/'
        }],
        options: {
            replacements: [
                {
                    pattern: /href="(\/fashion\/rba\/girlsaward\/css\/)/g,
                    replacement: 'href="http://event.rakuten.co.jp$1'
                },
                {
                    pattern: /src="(\/fashion\/rba\/girlsaward\/js\/)/g,
                    replacement: 'src="http://event.rakuten.co.jp$1'
                },
                {
                    pattern: /href="(\/smart\/rba\/girlsaward\/css\/)/g,
                    replacement: 'href="http://event.rakuten.co.jp$1'
                },
                {
                    pattern: /src="(\/smart\/rba\/girlsaward\/js\/)/g,
                    replacement: 'src="http://event.rakuten.co.jp$1'
                }
            ]
        }
    },

};