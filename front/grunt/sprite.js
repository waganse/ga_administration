// Generate CSS sprites
//
// grunt-spritesmith: <https://github.com/Ensighten/grunt-spritesmith>

'use strict';

module.exports = {

  compile: {
    src: '<%= path.sprites %>/*.png',
    dest: '<%= path.images %>/sprite.png',
    destCss: '<%= path.styles %>/_sprite/_sprite.styl',
    algorithm: 'binary-tree',
    padding: 2,
    cssOpts: {
      cssClass: function (item) {
          return '.' + item.name;
      }
    }
  }

};
