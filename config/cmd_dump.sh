#!/bin/sh

mysqldump --set-gtid-purged=OFF --user=ga_admin --password=ga_admin --host=cwd-web --protocol=tcp --port=3306 --default-character-set=utf8 --skip-triggers "ga_admin_new" > ./config/sql/db_bkup.sql