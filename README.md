# Configration

## Initial configuration

### Pre-requisit
PHP5.3 or upper version

### Install MAMP
* Install MAMP (https://www.mamp.info/en/downloads/)
* Check apache port # whether it conflicts with other processes

> If you would like to install the system on your exsisting Apache environment, you need to modify a configuration file indicating the system root path.

```
/ga_administration/fuel/app/config/config.php
return array(
    'base_url'  => '/ga_administration/',  => this means the url is "http://<domain>/ga_administration/"
```

### Clone repository & setup
* Clone repository (https://git.rakuten-it.com/projects/CWD_GM/repos/ga_administration/browse) to /Applications/MAMP/htdocs/ (It is highly recommended to change the DocumentRoot)

```
cd /Applications/MAMP/htdocs/
git clone ssh://git@git.rakuten-it.com:7999/cwd_gm/ga_administration.git
```

* Run "php composer.phar update" in the cloned ga_administration/ directory

```
cd ga_administration
php composer.phar update
```

* Run "./config/init.sh" in the ga_administration/ directory

```
./config/init.sh
```

* Modify "/ga_admininstration/fuel/packages/parser/config/parser.php" (L58 View_Twig => array( 'auto_encode' => false, ))

```
'View_Twig' => array(
    'auto_encode' => false,
    'views_paths' => array(APPPATH.'views'),
    'delimiters'  => array(
        'tag_block'    => array('left' => '{%', 'right' => '%}'),
        'tag_comment'  => array('left' => '{#', 'right' => '#}'),
        'tag_variable' => array('left' => '{{', 'right' => '}}'),
    ),
    'environment' => array(
        'debug'               => false,
        'charset'             => 'utf-8',
        'base_template_class' => 'Twig_Template',
        'cache'               => APPPATH.'cache'.DS.'twig'.DS,
        'auto_reload'                                                    => true,
        'strict_variables'    => false,
        'autoescape'          => false,
        'optimizations'       => -1,
    ),
    'extensions' => array(
        'Twig_Fuel_Extension',
    ),
),
```

### Database setup
* Access phpMyAdmin (http://localhost:8888/phpMyAdmin/)
* Click "Import" tab and import "/ga_administration/config/init.sql"
* Set a correct MySQL port number in "/ga_administration/fuel/conifig/db.php" The port # can be checked in MAMP preferences window.

### Front-end dev environment
* Run "npm install" & "bower install" on both "/ga_administration/front" & "/ga_administration/work"
* If you get error on "npm install" about "node-sass", please downgrade your node version.

```
nodebrew ls-remote
nodebrew install-binary v0.12.7
nodebrew use v0.10.32
```

### Check
* Access "http://localhost:8888/ga_administration/", then the login page appears!
* Login with administrator (username: admin, password: adminpass)

## Administration
* Follow the instruction indicated in http://localhost:8888/ga_administration/manual/.

## Directory

```
ga_admininstration
├── assets  <= UI srcs for the system (build from work/)
│   ├── css
│   ├── img
│   └── js
├── config  <= config files for the system
├── front  <= Girls Award Top & Brand pages (JP, EN, CN, TW) front-end sources based on RFF
├── fuel  <= PHP sources for the sytem (MVC)
│   ├── app
│   │   ├── classes
│   │   │   ├── controller
│   │   │   ├── model
│   │   ├── config
│   │   └── views
└── work  <= Administration system front-end sources based on Gulp
```





